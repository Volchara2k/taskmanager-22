package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Project;
import ru.renessans.jvschool.volkov.tm.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectViewByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_VIEW_BY_ID = "project-view-by-id";

    @NotNull
    private static final String DESC_PROJECT_VIEW_BY_ID = "просмотреть проект по идентификатору";

    @NotNull
    private static final String NOTIFY_PROJECT_VIEW_BY_ID_MSG =
            "Для отображения проекта по идентификатору введите идентификатор проекта из списка.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_VIEW_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_VIEW_BY_ID;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_PROJECT_VIEW_BY_ID_MSG);
        @NotNull final String id = ViewUtil.getLine();

        @NotNull final ProjectEndpoint projectEndpoint = super.serviceLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.getProjectById(opened, id);
        ViewUtil.print(project);
    }

}