package ru.renessans.jvschool.volkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DeveloperAboutCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_ABOUT = "about";

    @NotNull
    private static final String ARG_ABOUT = "-a";

    @NotNull
    private static final String DESC_ABOUT = "вывод информации о разработчике";

    @NotNull
    private static final String NOTIFY_ABOUT = "%s - разработчик; \n%s - почта.";

    @NotNull
    private static final String DEVELOPER = "Valery Volkov";

    @NotNull
    private static final String DEVELOPER_MAIL = "volkov.valery2013@yandex.ru";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_ABOUT;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARG_ABOUT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_ABOUT;
    }

    @Override
    public void execute() {
        ViewUtil.print(String.format(NOTIFY_ABOUT, DEVELOPER, DEVELOPER_MAIL));
    }

}