package ru.renessans.jvschool.volkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.constant.ControlDataConst;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ApplicationExitCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_EXIT = ControlDataConst.EXIT_FACTOR;

    @NotNull
    private static final String DESC_EXIT = "закрыть приложение";

    @NotNull
    private static final String NOTIFY_EXIT = "Выход из приложения!";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_EXIT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_EXIT;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_EXIT);
    }

}