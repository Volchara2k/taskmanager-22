package ru.renessans.jvschool.volkov.tm.exception.illegal;

import org.jetbrains.annotations.NotNull;

public final class IllegalInstantiationException extends RuntimeException {

    @NotNull
    private static final String HASH_ALGORITHM_ILLEGAL =
            "Ошибка! Параметр \"объект класса команда\" является нелегальным и не может быть создан!\n";

    public IllegalInstantiationException() {
        super(HASH_ALGORITHM_ILLEGAL);
    }

    public IllegalInstantiationException(final Throwable cause) {
        super(cause);
    }

}