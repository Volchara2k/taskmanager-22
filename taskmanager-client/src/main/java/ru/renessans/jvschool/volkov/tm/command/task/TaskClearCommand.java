package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.Task;
import ru.renessans.jvschool.volkov.tm.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_CLEAR = "task-clear";

    @NotNull
    private static final String DESC_TASK_CLEAR = "очистить все задачи";

    @NotNull
    private static final String NOTIFY_TASK_CLEAR = "Производится очистка списка задач...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_CLEAR;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_TASK_CLEAR);
        @NotNull final TaskEndpoint taskEndpoint = super.serviceLocator.getTaskEndpoint();

        @Nullable final Collection<Task> tasks = taskEndpoint.deleteAllTasks(opened);
        ViewUtil.print(tasks);
    }

}