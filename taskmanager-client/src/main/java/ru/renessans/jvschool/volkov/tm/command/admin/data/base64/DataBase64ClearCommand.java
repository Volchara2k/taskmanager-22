package ru.renessans.jvschool.volkov.tm.command.admin.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataBase64ClearCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_BASE64_CLEAR = "data-base64-clear";

    @NotNull
    private static final String DESC_BASE64_CLEAR = "очистить base64 данные";

    @NotNull
    private static final String NOTIFY_BASE64_CLEAR = "Происходит процесс очищения base64 данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BASE64_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BASE64_CLEAR;
    }

    @SneakyThrows
    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_BASE64_CLEAR);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminDataInterChangeEndpoint adminInterChangeEndpoint =
                super.serviceLocator.getAdminInterChangeEndpoint();
        final boolean removeState = adminInterChangeEndpoint.dataBase64Clear(opened);
        ViewUtil.print(removeState);
    }

}