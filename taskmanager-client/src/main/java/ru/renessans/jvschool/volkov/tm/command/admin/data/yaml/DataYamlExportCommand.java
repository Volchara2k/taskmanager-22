package ru.renessans.jvschool.volkov.tm.command.admin.data.yaml;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Domain;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataYamlExportCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_YAML_EXPORT = "data-yaml-export";

    @NotNull
    private static final String DESC_YAML_EXPORT = "экспортировать домен в yaml вид";

    @NotNull
    private static final String NOTIFY_YAML_EXPORT = "Происходит процесс выгрузки домена в yaml вид...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_YAML_EXPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_YAML_EXPORT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_YAML_EXPORT);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminDataInterChangeEndpoint adminInterChangeEndpoint =
                super.serviceLocator.getAdminInterChangeEndpoint();
        @NotNull final Domain domain = adminInterChangeEndpoint.exportDataYaml(opened);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}