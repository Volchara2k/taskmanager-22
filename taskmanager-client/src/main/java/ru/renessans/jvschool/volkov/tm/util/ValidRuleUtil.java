package ru.renessans.jvschool.volkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@UtilityClass
public final class ValidRuleUtil {

    public boolean isNullOrEmpty(@Nullable final String string) {
        return string == null || string.isEmpty();
    }

    public boolean isNullOrEmpty(@Nullable final String... strings) {
        return strings == null || strings.length < 1;
    }

    public boolean isNullOrEmpty(@Nullable final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

}