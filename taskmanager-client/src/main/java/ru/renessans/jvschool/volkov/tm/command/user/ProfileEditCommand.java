package ru.renessans.jvschool.volkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.User;
import ru.renessans.jvschool.volkov.tm.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProfileEditCommand extends AbstractAuthCommand {

    @NotNull
    private static final String CMD_EDIT_PROFILE = "edit-profile";

    @NotNull
    private static final String DESC_EDIT_PROFILE = "изменить данные пользователя";

    @NotNull
    private static final String NOTIFY_EDIT_PROFILE =
            "Для обновления данных пользователя введите его имя или имя с фамилией: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_EDIT_PROFILE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_EDIT_PROFILE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_EDIT_PROFILE);
        @NotNull final ICurrentSessionService currentSession = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSession.get();
        @NotNull final String firstName = ViewUtil.getLine();

        @NotNull final UserEndpoint userEndpoint = super.serviceLocator.getUserEndpoint();
        @Nullable final User user = userEndpoint.editProfile(opened, firstName);
        ViewUtil.print(user);
    }

}