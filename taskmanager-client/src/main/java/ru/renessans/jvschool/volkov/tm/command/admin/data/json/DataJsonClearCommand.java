package ru.renessans.jvschool.volkov.tm.command.admin.data.json;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataJsonClearCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_JSON_CLEAR = "data-json-clear";

    @NotNull
    private static final String DESC_JSON_CLEAR = "очистить json данные";

    @NotNull
    private static final String NOTIFY_JSON_CLEAR = "Происходит процесс очищения json данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_JSON_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_JSON_CLEAR;
    }

    @SneakyThrows
    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_JSON_CLEAR);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminDataInterChangeEndpoint adminInterChangeEndpoint =
                super.serviceLocator.getAdminInterChangeEndpoint();
        final boolean removeState = adminInterChangeEndpoint.dataJsonClear(opened);

        ViewUtil.print(removeState);
    }

}