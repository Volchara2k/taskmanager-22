package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.Task;
import ru.renessans.jvschool.volkov.tm.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskDeleteByTitleCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_TITLE = "task-delete-by-title";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_TITLE = "удалить задачу по заголовку";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_TITLE =
            "Для удаления задачи по заголовку введите имя заголовок из списка ниже.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_TITLE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_TITLE;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_TASK_DELETE_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();

        @NotNull final TaskEndpoint taskEndpoint = super.serviceLocator.getTaskEndpoint();
        @Nullable final Task task = taskEndpoint.deleteTaskByTitle(opened, title);
        ViewUtil.print(task);
    }

}