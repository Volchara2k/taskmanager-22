package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Project;
import ru.renessans.jvschool.volkov.tm.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectDeleteByTitleCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_DELETE_BY_TITLE = "project-delete-by-title";

    @NotNull
    private static final String DESC_PROJECT_DELETE_BY_TITLE = "удалить проект по заголовку";

    @NotNull
    private static final String NOTIFY_PROJECT_DELETE_BY_TITLE =
            "Для удаления заголовка по имени введите заголовок проекта из списка.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_DELETE_BY_TITLE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_DELETE_BY_TITLE;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_PROJECT_DELETE_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();

        @NotNull final ProjectEndpoint projectEndpoint = super.serviceLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.deleteProjectByTitle(opened, title);
        ViewUtil.print(project);
    }

}