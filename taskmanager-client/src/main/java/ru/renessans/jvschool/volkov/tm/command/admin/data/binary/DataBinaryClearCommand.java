package ru.renessans.jvschool.volkov.tm.command.admin.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataBinaryClearCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_BIN_CLEAR = "data-bin-clear";

    @NotNull
    private static final String DESC_BIN_CLEAR = "очистить бинарные данные";

    @NotNull
    private static final String NOTIFY_BIN_CLEAR = "Происходит процесс очищения бинарных данных...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BIN_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BIN_CLEAR;
    }

    @SneakyThrows
    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_BIN_CLEAR);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminDataInterChangeEndpoint adminInterChangeEndpoint =
                super.serviceLocator.getAdminInterChangeEndpoint();
        final boolean removeState = adminInterChangeEndpoint.dataBinClear(opened);
        ViewUtil.print(removeState);
    }

}