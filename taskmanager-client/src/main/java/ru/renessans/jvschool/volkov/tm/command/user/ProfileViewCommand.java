package ru.renessans.jvschool.volkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.User;
import ru.renessans.jvschool.volkov.tm.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProfileViewCommand extends AbstractAuthCommand {

    @NotNull
    private static final String CMD_VIEW_PROFILE = "view-profile";

    @NotNull
    private static final String DESC_VIEW_PROFILE = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_VIEW_PROFILE = "Информация о текущем профиле пользователя: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_VIEW_PROFILE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_VIEW_PROFILE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_VIEW_PROFILE);
        @NotNull final ICurrentSessionService currentSession = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSession.get();
        @NotNull final UserEndpoint userEndpoint = super.serviceLocator.getUserEndpoint();
        @Nullable final User user = userEndpoint.getUser(opened);
        ViewUtil.print(user);
    }

}