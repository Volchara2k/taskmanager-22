package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Project;
import ru.renessans.jvschool.volkov.tm.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectViewByTitleCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_VIEW_BY_TITLE = "project-view-by-title";

    @NotNull
    private static final String DESC_PROJECT_VIEW_BY_TITLE = "просмотреть проект по заголовку";

    @NotNull
    private static final String NOTIFY_PROJECT_VIEW_BY_TITLE_MSG =
            "Для отображения проекта по заголовку введите заголовок проекта из списка.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_VIEW_BY_TITLE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_VIEW_BY_TITLE;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_PROJECT_VIEW_BY_TITLE_MSG);
        @NotNull final String title = ViewUtil.getLine();

        @NotNull final ProjectEndpoint projectEndpoint = super.serviceLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.getProjectByTitle(opened, title);
        ViewUtil.print(project);
    }

}