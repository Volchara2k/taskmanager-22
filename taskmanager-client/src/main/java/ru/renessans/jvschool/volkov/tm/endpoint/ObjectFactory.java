package ru.renessans.jvschool.volkov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.renessans.jvschool.volkov.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataBase64Clear_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataBase64Clear");
    private final static QName _DataBase64ClearResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataBase64ClearResponse");
    private final static QName _DataBinClear_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataBinClear");
    private final static QName _DataBinClearResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataBinClearResponse");
    private final static QName _DataJsonClear_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataJsonClear");
    private final static QName _DataJsonClearResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataJsonClearResponse");
    private final static QName _DataXmlClear_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataXmlClear");
    private final static QName _DataXmlClearResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataXmlClearResponse");
    private final static QName _DataYamlClear_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataYamlClear");
    private final static QName _DataYamlClearResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "dataYamlClearResponse");
    private final static QName _ExportDataBase64_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataBase64");
    private final static QName _ExportDataBase64Response_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataBase64Response");
    private final static QName _ExportDataBin_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataBin");
    private final static QName _ExportDataBinResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataBinResponse");
    private final static QName _ExportDataJson_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataJson");
    private final static QName _ExportDataJsonResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataJsonResponse");
    private final static QName _ExportDataXml_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataXml");
    private final static QName _ExportDataXmlResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataXmlResponse");
    private final static QName _ExportDataYaml_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataYaml");
    private final static QName _ExportDataYamlResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "exportDataYamlResponse");
    private final static QName _ImportDataBase64_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataBase64");
    private final static QName _ImportDataBase64Response_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataBase64Response");
    private final static QName _ImportDataBin_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataBin");
    private final static QName _ImportDataBinResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataBinResponse");
    private final static QName _ImportDataJson_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataJson");
    private final static QName _ImportDataJsonResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataJsonResponse");
    private final static QName _ImportDataXml_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataXml");
    private final static QName _ImportDataXmlResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataXmlResponse");
    private final static QName _ImportDataYaml_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataYaml");
    private final static QName _ImportDataYamlResponse_QNAME = new QName("http://endpoint.tm.volkov.jvschool.renessans.ru/", "importDataYamlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.renessans.jvschool.volkov.tm.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataBase64Clear }
     */
    public DataBase64Clear createDataBase64Clear() {
        return new DataBase64Clear();
    }

    /**
     * Create an instance of {@link DataBase64ClearResponse }
     */
    public DataBase64ClearResponse createDataBase64ClearResponse() {
        return new DataBase64ClearResponse();
    }

    /**
     * Create an instance of {@link DataBinClear }
     */
    public DataBinClear createDataBinClear() {
        return new DataBinClear();
    }

    /**
     * Create an instance of {@link DataBinClearResponse }
     */
    public DataBinClearResponse createDataBinClearResponse() {
        return new DataBinClearResponse();
    }

    /**
     * Create an instance of {@link DataJsonClear }
     */
    public DataJsonClear createDataJsonClear() {
        return new DataJsonClear();
    }

    /**
     * Create an instance of {@link DataJsonClearResponse }
     */
    public DataJsonClearResponse createDataJsonClearResponse() {
        return new DataJsonClearResponse();
    }

    /**
     * Create an instance of {@link DataXmlClear }
     */
    public DataXmlClear createDataXmlClear() {
        return new DataXmlClear();
    }

    /**
     * Create an instance of {@link DataXmlClearResponse }
     */
    public DataXmlClearResponse createDataXmlClearResponse() {
        return new DataXmlClearResponse();
    }

    /**
     * Create an instance of {@link DataYamlClear }
     */
    public DataYamlClear createDataYamlClear() {
        return new DataYamlClear();
    }

    /**
     * Create an instance of {@link DataYamlClearResponse }
     */
    public DataYamlClearResponse createDataYamlClearResponse() {
        return new DataYamlClearResponse();
    }

    /**
     * Create an instance of {@link ExportDataBase64 }
     */
    public ExportDataBase64 createExportDataBase64() {
        return new ExportDataBase64();
    }

    /**
     * Create an instance of {@link ExportDataBase64Response }
     */
    public ExportDataBase64Response createExportDataBase64Response() {
        return new ExportDataBase64Response();
    }

    /**
     * Create an instance of {@link ExportDataBin }
     */
    public ExportDataBin createExportDataBin() {
        return new ExportDataBin();
    }

    /**
     * Create an instance of {@link ExportDataBinResponse }
     */
    public ExportDataBinResponse createExportDataBinResponse() {
        return new ExportDataBinResponse();
    }

    /**
     * Create an instance of {@link ExportDataJson }
     */
    public ExportDataJson createExportDataJson() {
        return new ExportDataJson();
    }

    /**
     * Create an instance of {@link ExportDataJsonResponse }
     */
    public ExportDataJsonResponse createExportDataJsonResponse() {
        return new ExportDataJsonResponse();
    }

    /**
     * Create an instance of {@link ExportDataXml }
     */
    public ExportDataXml createExportDataXml() {
        return new ExportDataXml();
    }

    /**
     * Create an instance of {@link ExportDataXmlResponse }
     */
    public ExportDataXmlResponse createExportDataXmlResponse() {
        return new ExportDataXmlResponse();
    }

    /**
     * Create an instance of {@link ExportDataYaml }
     */
    public ExportDataYaml createExportDataYaml() {
        return new ExportDataYaml();
    }

    /**
     * Create an instance of {@link ExportDataYamlResponse }
     */
    public ExportDataYamlResponse createExportDataYamlResponse() {
        return new ExportDataYamlResponse();
    }

    /**
     * Create an instance of {@link ImportDataBase64 }
     */
    public ImportDataBase64 createImportDataBase64() {
        return new ImportDataBase64();
    }

    /**
     * Create an instance of {@link ImportDataBase64Response }
     */
    public ImportDataBase64Response createImportDataBase64Response() {
        return new ImportDataBase64Response();
    }

    /**
     * Create an instance of {@link ImportDataBin }
     */
    public ImportDataBin createImportDataBin() {
        return new ImportDataBin();
    }

    /**
     * Create an instance of {@link ImportDataBinResponse }
     */
    public ImportDataBinResponse createImportDataBinResponse() {
        return new ImportDataBinResponse();
    }

    /**
     * Create an instance of {@link ImportDataJson }
     */
    public ImportDataJson createImportDataJson() {
        return new ImportDataJson();
    }

    /**
     * Create an instance of {@link ImportDataJsonResponse }
     */
    public ImportDataJsonResponse createImportDataJsonResponse() {
        return new ImportDataJsonResponse();
    }

    /**
     * Create an instance of {@link ImportDataXml }
     */
    public ImportDataXml createImportDataXml() {
        return new ImportDataXml();
    }

    /**
     * Create an instance of {@link ImportDataXmlResponse }
     */
    public ImportDataXmlResponse createImportDataXmlResponse() {
        return new ImportDataXmlResponse();
    }

    /**
     * Create an instance of {@link ImportDataYaml }
     */
    public ImportDataYaml createImportDataYaml() {
        return new ImportDataYaml();
    }

    /**
     * Create an instance of {@link ImportDataYamlResponse }
     */
    public ImportDataYamlResponse createImportDataYamlResponse() {
        return new ImportDataYamlResponse();
    }

    /**
     * Create an instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link Domain }
     */
    public Domain createDomain() {
        return new Domain();
    }

    /**
     * Create an instance of {@link Project }
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link Task }
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link User }
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64Clear }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataBase64Clear")
    public JAXBElement<DataBase64Clear> createDataBase64Clear(DataBase64Clear value) {
        return new JAXBElement<DataBase64Clear>(_DataBase64Clear_QNAME, DataBase64Clear.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBase64ClearResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataBase64ClearResponse")
    public JAXBElement<DataBase64ClearResponse> createDataBase64ClearResponse(DataBase64ClearResponse value) {
        return new JAXBElement<DataBase64ClearResponse>(_DataBase64ClearResponse_QNAME, DataBase64ClearResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinClear }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataBinClear")
    public JAXBElement<DataBinClear> createDataBinClear(DataBinClear value) {
        return new JAXBElement<DataBinClear>(_DataBinClear_QNAME, DataBinClear.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataBinClearResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataBinClearResponse")
    public JAXBElement<DataBinClearResponse> createDataBinClearResponse(DataBinClearResponse value) {
        return new JAXBElement<DataBinClearResponse>(_DataBinClearResponse_QNAME, DataBinClearResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonClear }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataJsonClear")
    public JAXBElement<DataJsonClear> createDataJsonClear(DataJsonClear value) {
        return new JAXBElement<DataJsonClear>(_DataJsonClear_QNAME, DataJsonClear.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataJsonClearResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataJsonClearResponse")
    public JAXBElement<DataJsonClearResponse> createDataJsonClearResponse(DataJsonClearResponse value) {
        return new JAXBElement<DataJsonClearResponse>(_DataJsonClearResponse_QNAME, DataJsonClearResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlClear }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataXmlClear")
    public JAXBElement<DataXmlClear> createDataXmlClear(DataXmlClear value) {
        return new JAXBElement<DataXmlClear>(_DataXmlClear_QNAME, DataXmlClear.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataXmlClearResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataXmlClearResponse")
    public JAXBElement<DataXmlClearResponse> createDataXmlClearResponse(DataXmlClearResponse value) {
        return new JAXBElement<DataXmlClearResponse>(_DataXmlClearResponse_QNAME, DataXmlClearResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlClear }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataYamlClear")
    public JAXBElement<DataYamlClear> createDataYamlClear(DataYamlClear value) {
        return new JAXBElement<DataYamlClear>(_DataYamlClear_QNAME, DataYamlClear.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataYamlClearResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "dataYamlClearResponse")
    public JAXBElement<DataYamlClearResponse> createDataYamlClearResponse(DataYamlClearResponse value) {
        return new JAXBElement<DataYamlClearResponse>(_DataYamlClearResponse_QNAME, DataYamlClearResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataBase64 }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataBase64")
    public JAXBElement<ExportDataBase64> createExportDataBase64(ExportDataBase64 value) {
        return new JAXBElement<ExportDataBase64>(_ExportDataBase64_QNAME, ExportDataBase64.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataBase64Response }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataBase64Response")
    public JAXBElement<ExportDataBase64Response> createExportDataBase64Response(ExportDataBase64Response value) {
        return new JAXBElement<ExportDataBase64Response>(_ExportDataBase64Response_QNAME, ExportDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataBin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataBin")
    public JAXBElement<ExportDataBin> createExportDataBin(ExportDataBin value) {
        return new JAXBElement<ExportDataBin>(_ExportDataBin_QNAME, ExportDataBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataBinResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataBinResponse")
    public JAXBElement<ExportDataBinResponse> createExportDataBinResponse(ExportDataBinResponse value) {
        return new JAXBElement<ExportDataBinResponse>(_ExportDataBinResponse_QNAME, ExportDataBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataJson }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataJson")
    public JAXBElement<ExportDataJson> createExportDataJson(ExportDataJson value) {
        return new JAXBElement<ExportDataJson>(_ExportDataJson_QNAME, ExportDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataJsonResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataJsonResponse")
    public JAXBElement<ExportDataJsonResponse> createExportDataJsonResponse(ExportDataJsonResponse value) {
        return new JAXBElement<ExportDataJsonResponse>(_ExportDataJsonResponse_QNAME, ExportDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataXml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataXml")
    public JAXBElement<ExportDataXml> createExportDataXml(ExportDataXml value) {
        return new JAXBElement<ExportDataXml>(_ExportDataXml_QNAME, ExportDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataXmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataXmlResponse")
    public JAXBElement<ExportDataXmlResponse> createExportDataXmlResponse(ExportDataXmlResponse value) {
        return new JAXBElement<ExportDataXmlResponse>(_ExportDataXmlResponse_QNAME, ExportDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataYaml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataYaml")
    public JAXBElement<ExportDataYaml> createExportDataYaml(ExportDataYaml value) {
        return new JAXBElement<ExportDataYaml>(_ExportDataYaml_QNAME, ExportDataYaml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportDataYamlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "exportDataYamlResponse")
    public JAXBElement<ExportDataYamlResponse> createExportDataYamlResponse(ExportDataYamlResponse value) {
        return new JAXBElement<ExportDataYamlResponse>(_ExportDataYamlResponse_QNAME, ExportDataYamlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataBase64 }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataBase64")
    public JAXBElement<ImportDataBase64> createImportDataBase64(ImportDataBase64 value) {
        return new JAXBElement<ImportDataBase64>(_ImportDataBase64_QNAME, ImportDataBase64.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataBase64Response }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataBase64Response")
    public JAXBElement<ImportDataBase64Response> createImportDataBase64Response(ImportDataBase64Response value) {
        return new JAXBElement<ImportDataBase64Response>(_ImportDataBase64Response_QNAME, ImportDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataBin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataBin")
    public JAXBElement<ImportDataBin> createImportDataBin(ImportDataBin value) {
        return new JAXBElement<ImportDataBin>(_ImportDataBin_QNAME, ImportDataBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataBinResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataBinResponse")
    public JAXBElement<ImportDataBinResponse> createImportDataBinResponse(ImportDataBinResponse value) {
        return new JAXBElement<ImportDataBinResponse>(_ImportDataBinResponse_QNAME, ImportDataBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataJson }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataJson")
    public JAXBElement<ImportDataJson> createImportDataJson(ImportDataJson value) {
        return new JAXBElement<ImportDataJson>(_ImportDataJson_QNAME, ImportDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataJsonResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataJsonResponse")
    public JAXBElement<ImportDataJsonResponse> createImportDataJsonResponse(ImportDataJsonResponse value) {
        return new JAXBElement<ImportDataJsonResponse>(_ImportDataJsonResponse_QNAME, ImportDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataXml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataXml")
    public JAXBElement<ImportDataXml> createImportDataXml(ImportDataXml value) {
        return new JAXBElement<ImportDataXml>(_ImportDataXml_QNAME, ImportDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataXmlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataXmlResponse")
    public JAXBElement<ImportDataXmlResponse> createImportDataXmlResponse(ImportDataXmlResponse value) {
        return new JAXBElement<ImportDataXmlResponse>(_ImportDataXmlResponse_QNAME, ImportDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataYaml }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataYaml")
    public JAXBElement<ImportDataYaml> createImportDataYaml(ImportDataYaml value) {
        return new JAXBElement<ImportDataYaml>(_ImportDataYaml_QNAME, ImportDataYaml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataYamlResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "importDataYamlResponse")
    public JAXBElement<ImportDataYamlResponse> createImportDataYamlResponse(ImportDataYamlResponse value) {
        return new JAXBElement<ImportDataYamlResponse>(_ImportDataYamlResponse_QNAME, ImportDataYamlResponse.class, null, value);
    }

}
