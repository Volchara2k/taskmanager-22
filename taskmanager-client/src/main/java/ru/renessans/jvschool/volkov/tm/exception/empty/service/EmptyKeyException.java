package ru.renessans.jvschool.volkov.tm.exception.empty.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyKeyException extends AbstractException {

    @NotNull
    private static final String EMPTY_KEY = "Ошибка! Параметр \"ключ значения\" является пустым или null!\n";

    public EmptyKeyException() {
        super(EMPTY_KEY);
    }

}