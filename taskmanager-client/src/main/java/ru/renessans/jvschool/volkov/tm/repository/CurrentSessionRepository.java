package ru.renessans.jvschool.volkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;

import java.util.Objects;

public final class CurrentSessionRepository extends AbstractRepository<Session> implements ICurrentSessionRepository {

    @NotNull
    @SneakyThrows
    @Override
    public Session getSession(@NotNull final String sessionId) {
        @Nullable Session opened = super.getRecordByKey(sessionId);
        if (Objects.isNull(opened)) throw new AccessFailureException("Сессия не найдена!");
        return opened;
    }

}