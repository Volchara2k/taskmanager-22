package ru.renessans.jvschool.volkov.tm.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class CommandAddFailureException extends AbstractException {

    @NotNull
    private static final String INCORRECT_COMMAND_REGISTRATION =
            "Ошибка! Сбой добавления команд: %s!\n";

    public CommandAddFailureException(@NotNull final String message) {
        super(String.format(INCORRECT_COMMAND_REGISTRATION, message));
    }

}