package ru.renessans.jvschool.volkov.tm.command.security;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.user.AbstractAuthCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserLogOutCommand extends AbstractAuthCommand {

    @NotNull
    private static final String CMD_LOG_OUT = "log-out";

    @Nullable
    private static final String DESC_LOG_OUT = "выйти из системы";

    @NotNull
    private static final String NOTIFY_LOG_OUT = "Производится выход пользователя из системы...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_LOG_OUT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_LOG_OUT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_LOG_OUT);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final SessionEndpoint sessionEndpoint = super.serviceLocator.getSessionEndpoint();
        final boolean isClosed = sessionEndpoint.closeSession(opened);
        currentSessionService.delete();
        ViewUtil.print(isClosed);
    }

}