package ru.renessans.jvschool.volkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.AuthenticationEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.PermissionValidState;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptySessionException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Arrays;
import java.util.Objects;

public final class CurrentSessionService extends AbstractService<Session> implements ICurrentSessionService {

    @Nullable
    private static String CURRENT_SESSION_ID;

    @NotNull
    private final ICurrentSessionRepository sessionRepository;

    @NotNull
    private final AuthenticationEndpoint authenticationEndpoint;

    public CurrentSessionService(
            @NotNull final ICurrentSessionRepository sessionRepository,
            @NotNull final AuthenticationEndpoint authenticationEndpoint
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.authenticationEndpoint = authenticationEndpoint;
    }

    @Nullable
    @SneakyThrows
    @Override
    public Session get() {
        if (ValidRuleUtil.isNullOrEmpty(CURRENT_SESSION_ID)) return null;
        return this.sessionRepository.getSession(CURRENT_SESSION_ID);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Session put(
            @Nullable final Session session
    ) {
        if (Objects.isNull(session)) throw new EmptySessionException();
        CURRENT_SESSION_ID = session.getId();
        return super.addRecord(session);
    }

    @SneakyThrows
    @Override
    public boolean delete() {
        if (ValidRuleUtil.isNullOrEmpty(CURRENT_SESSION_ID)) return false;
        @Nullable final Session closeable = this.sessionRepository.getSession(CURRENT_SESSION_ID);
        CURRENT_SESSION_ID = null;
        return super.wasDeletedRecord(closeable);
    }

    @NotNull
    @SneakyThrows
    @Override
    public PermissionValidState verifyPermissions(@Nullable final UserRole[] commandRoles) {
        if (Objects.isNull(commandRoles)) return PermissionValidState.SUCCESS;

        @Nullable final Session opened = get();
        @NotNull final UserRole userRole = authenticationEndpoint.getUserRole(opened);

        boolean containsTypes = Arrays.asList(commandRoles).contains(userRole);
        if (containsTypes) return PermissionValidState.SUCCESS;

        final boolean isUser = (userRole == UserRole.USER);
        if (isUser) return PermissionValidState.NO_ACCESS_RIGHTS;
        else return PermissionValidState.NEED_LOG_IN;
    }

}