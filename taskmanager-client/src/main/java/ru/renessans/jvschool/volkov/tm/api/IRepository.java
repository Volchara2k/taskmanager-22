package ru.renessans.jvschool.volkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.endpoint.AbstractModel;

import java.util.Collection;

public interface IRepository<E extends AbstractModel> {

    @NotNull
    E addRecord(@NotNull E value);

    @Nullable
    E updateRecord(@NotNull E value);

    @NotNull
    Collection<E> getAllRecords();

    @Nullable
    E getRecordByKey(@NotNull String key);

    @NotNull
    Collection<E> deleteAllRecords();

    @Nullable
    E deleteRecordByKey(@NotNull String key);

    @Nullable
    E deleteRecord(@NotNull E value);

    @NotNull
    Collection<E> setAllRecords(@NotNull Collection<E> values);

    boolean wasDeletedRecord(@NotNull E value);

    boolean wasDeletedAllRecords();

}