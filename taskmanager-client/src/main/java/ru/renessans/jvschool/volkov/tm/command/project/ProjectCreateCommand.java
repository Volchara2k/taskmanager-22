package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Project;
import ru.renessans.jvschool.volkov.tm.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_CREATE = "project-create";

    @NotNull
    private static final String DESC_PROJECT_CREATE = "добавить новый проект";

    @NotNull
    private static final String NOTIFY_PROJECT_CREATE =
            "Для создания проекта введите его заголовок или заголовок с описанием.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_CREATE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_CREATE;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_PROJECT_CREATE);
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final ProjectEndpoint projectEndpoint = super.serviceLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.addProject(opened, title, description);
        ViewUtil.print(project);
    }

}