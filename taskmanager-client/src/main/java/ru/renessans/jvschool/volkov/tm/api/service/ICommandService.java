package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @NotNull
    Collection<AbstractCommand> initCommands(@Nullable IServiceLocator serviceLocator);

    @Nullable
    Collection<AbstractCommand> getAllCommands();

    @Nullable
    Collection<AbstractCommand> getAllTerminalCommands();

    @Nullable
    Collection<AbstractCommand> getAllArgumentCommands();

    @Nullable
    AbstractCommand getTerminalCommand(@Nullable String command);

    @Nullable
    AbstractCommand getArgumentCommand(@Nullable String command);

}