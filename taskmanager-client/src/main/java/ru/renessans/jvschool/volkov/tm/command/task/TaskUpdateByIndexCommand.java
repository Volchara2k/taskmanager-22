package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.Task;
import ru.renessans.jvschool.volkov.tm.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    @NotNull
    private static final String DESC_TASK_UPDATE_BY_INDEX = "обновить задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_UPDATE_BY_INDEX =
            "Для обновления задачи по индексу введите индекс задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_UPDATE_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_TASK_UPDATE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final TaskEndpoint taskEndpoint = super.serviceLocator.getTaskEndpoint();
        @Nullable final Task updatedTask = taskEndpoint.updateTaskByIndex(opened, index, title, description);
        ViewUtil.print(updatedTask);
    }

}