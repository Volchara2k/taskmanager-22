package ru.renessans.jvschool.volkov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.constant.ControlDataConst;
import ru.renessans.jvschool.volkov.tm.endpoint.*;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.tm.repository.CommandRepository;
import ru.renessans.jvschool.volkov.tm.repository.CurrentSessionRepository;
import ru.renessans.jvschool.volkov.tm.service.CommandService;
import ru.renessans.jvschool.volkov.tm.service.CurrentSessionService;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Objects;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();


    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();


    @NotNull
    private final AdminDataInterChangeEndpointService adminDomainEndpointService = new AdminDataInterChangeEndpointService();

    @NotNull
    private final AdminDataInterChangeEndpoint adminInterChangeEndpoint =
            adminDomainEndpointService.getAdminDataInterChangeEndpointPort();


    @NotNull
    private final AuthenticationEndpointService authenticationEndpointService = new AuthenticationEndpointService();

    @NotNull
    private final AuthenticationEndpoint authenticationEndpoint = authenticationEndpointService.getAuthenticationEndpointPort();


    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();


    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();


    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();


    @NotNull
    private final ICurrentSessionRepository sessionRepository = new CurrentSessionRepository();

    @NotNull
    private final ICurrentSessionService sessionService =
            new CurrentSessionService(sessionRepository, authenticationEndpoint);


    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);


    @NotNull
    @Override
    public AuthenticationEndpoint getAuthenticationEndpoint() {
        return this.authenticationEndpoint;
    }

    @NotNull
    @Override
    public AdminEndpoint getAdminEndpoint() {
        return this.adminEndpoint;
    }

    @NotNull
    @Override
    public AdminDataInterChangeEndpoint getAdminInterChangeEndpoint() {
        return this.adminInterChangeEndpoint;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return this.sessionEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return this.userEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return this.projectEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return this.taskEndpoint;
    }

    @NotNull
    @Override
    public ICurrentSessionService getCurrentSession() {
        return this.sessionService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return this.commandService;
    }

    {
        commandService.initCommands(this);
    }

    public void run(@Nullable final String... args) {
        final boolean emptyArgs = ValidRuleUtil.isNullOrEmpty(args);
        if (emptyArgs) terminalCommandPrintLoop();
        else argumentPrint(Objects.requireNonNull(args[0]));
    }

    private void terminalCommandPrintLoop() {
        @NotNull String command = "";
        while (!ControlDataConst.EXIT_FACTOR.equals(command)) {
            try {
                command = ScannerUtil.getLine();
                @Nullable final AbstractCommand abstractCommand = this.commandService.getTerminalCommand(command);
                if (Objects.isNull(abstractCommand)) throw new UnknownCommandException(command);
                executeCommand(abstractCommand);
            } catch (final Exception e) {
                System.err.print(e.getMessage());
            }
        }
    }

    private void argumentPrint(@NotNull final String arg) {
        try {
            @Nullable final AbstractCommand abstractCommand = this.commandService.getArgumentCommand(arg);
            if (Objects.isNull(abstractCommand)) throw new UnknownCommandException(arg);
            executeCommand(abstractCommand);
        } catch (final Exception e) {
            System.err.print(e.getMessage());
        }
    }

    @SneakyThrows
    private void executeCommand(@NotNull final AbstractCommand abstractCommand) {
        @NotNull final PermissionValidState permissionState =
                sessionService.verifyPermissions(abstractCommand.permissions());
        if (permissionState != PermissionValidState.SUCCESS) throw new AccessFailureException(permissionState.value());
        abstractCommand.execute();
    }

}