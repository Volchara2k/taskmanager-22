package ru.renessans.jvschool.volkov.tm.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IRepository;
import ru.renessans.jvschool.volkov.tm.api.IService;
import ru.renessans.jvschool.volkov.tm.endpoint.AbstractModel;
import ru.renessans.jvschool.volkov.tm.exception.empty.service.EmptyKeyException;
import ru.renessans.jvschool.volkov.tm.exception.empty.service.EmptyValueException;
import ru.renessans.jvschool.volkov.tm.exception.empty.service.EmptyValuesException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> setAllRecords(@Nullable final Collection<E> values) {
        if (ValidRuleUtil.isNullOrEmpty(values)) throw new EmptyValueException();
        deleteAllRecords();
        return this.repository.setAllRecords(values);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E addRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new EmptyValuesException();
        return this.repository.addRecord(value);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E updateRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new EmptyValueException();
        return this.repository.updateRecord(value);
    }

    @NotNull
    @Override
    public Collection<E> getAllRecords() {
        return this.repository.getAllRecords();
    }

    @Nullable
    @SneakyThrows
    @Override
    public E getRecordByKey(@Nullable final String key) {
        if (ValidRuleUtil.isNullOrEmpty(key)) throw new EmptyKeyException();
        return this.repository.getRecordByKey(key);
    }

    @NotNull
    @Override
    public Collection<E> deleteAllRecords() {
        return repository.deleteAllRecords();
    }

    @Nullable
    @SneakyThrows
    @Override
    public E deleteRecordByKey(@Nullable final String key) {
        if (ValidRuleUtil.isNullOrEmpty(key)) throw new EmptyKeyException();
        return this.repository.deleteRecordByKey(key);
    }

    @Nullable
    @SneakyThrows
    @Override
    public E deleteRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new EmptyValueException();
        return this.repository.deleteRecord(value);
    }

    @SneakyThrows
    @Override
    public boolean wasDeletedRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new EmptyValueException();
        return this.repository.wasDeletedRecord(value);
    }

    @Override
    public boolean wasDeletedAllRecords() {
        return this.repository.wasDeletedAllRecords();
    }

}