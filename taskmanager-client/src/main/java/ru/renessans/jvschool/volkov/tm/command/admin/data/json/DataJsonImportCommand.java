package ru.renessans.jvschool.volkov.tm.command.admin.data.json;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Domain;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataJsonImportCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_JSON_IMPORT = "data-json-import";

    @NotNull
    private static final String DESC_JSON_IMPORT = "импортировать домен из json вида";

    @NotNull
    private static final String NOTIFY_JSON_IMPORT = "Происходит процесс загрузки домена из json вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_JSON_IMPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_JSON_IMPORT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_JSON_IMPORT);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminDataInterChangeEndpoint adminInterChangeEndpoint =
                super.serviceLocator.getAdminInterChangeEndpoint();
        @NotNull final Domain domain = adminInterChangeEndpoint.importDataJson(opened);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}