package ru.renessans.jvschool.volkov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-05T09:42:17.036+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", name = "AdminDataInterChangeEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminDataInterChangeEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataYamlClearRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataYamlClearResponse")
    @RequestWrapper(localName = "dataYamlClear", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataYamlClear")
    @ResponseWrapper(localName = "dataYamlClearResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataYamlClearResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean dataYamlClear(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataJsonRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataJsonResponse")
    @RequestWrapper(localName = "exportDataJson", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataJson")
    @ResponseWrapper(localName = "exportDataJsonResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataJsonResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain exportDataJson(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataBinRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataBinResponse")
    @RequestWrapper(localName = "importDataBin", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataBin")
    @ResponseWrapper(localName = "importDataBinResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataBinResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain importDataBin(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataXmlClearRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataXmlClearResponse")
    @RequestWrapper(localName = "dataXmlClear", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataXmlClear")
    @ResponseWrapper(localName = "dataXmlClearResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataXmlClearResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean dataXmlClear(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataBase64Request", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataBase64Response")
    @RequestWrapper(localName = "exportDataBase64", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataBase64")
    @ResponseWrapper(localName = "exportDataBase64Response", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataBase64Response")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain exportDataBase64(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataBinRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataBinResponse")
    @RequestWrapper(localName = "exportDataBin", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataBin")
    @ResponseWrapper(localName = "exportDataBinResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataBinResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain exportDataBin(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataJsonClearRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataJsonClearResponse")
    @RequestWrapper(localName = "dataJsonClear", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataJsonClear")
    @ResponseWrapper(localName = "dataJsonClearResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataJsonClearResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean dataJsonClear(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataBase64Request", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataBase64Response")
    @RequestWrapper(localName = "importDataBase64", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataBase64")
    @ResponseWrapper(localName = "importDataBase64Response", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataBase64Response")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain importDataBase64(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataXmlRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataXmlResponse")
    @RequestWrapper(localName = "importDataXml", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataXml")
    @ResponseWrapper(localName = "importDataXmlResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataXmlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain importDataXml(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataYamlRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataYamlResponse")
    @RequestWrapper(localName = "importDataYaml", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataYaml")
    @ResponseWrapper(localName = "importDataYamlResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataYamlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain importDataYaml(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataBase64ClearRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataBase64ClearResponse")
    @RequestWrapper(localName = "dataBase64Clear", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataBase64Clear")
    @ResponseWrapper(localName = "dataBase64ClearResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataBase64ClearResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean dataBase64Clear(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataBinClearRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/dataBinClearResponse")
    @RequestWrapper(localName = "dataBinClear", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataBinClear")
    @ResponseWrapper(localName = "dataBinClearResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.DataBinClearResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean dataBinClear(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataJsonRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/importDataJsonResponse")
    @RequestWrapper(localName = "importDataJson", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataJson")
    @ResponseWrapper(localName = "importDataJsonResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ImportDataJsonResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain importDataJson(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataXmlRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataXmlResponse")
    @RequestWrapper(localName = "exportDataXml", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataXml")
    @ResponseWrapper(localName = "exportDataXmlResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataXmlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain exportDataXml(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataYamlRequest", output = "http://endpoint.tm.volkov.jvschool.renessans.ru/AdminDataInterChangeEndpoint/exportDataYamlResponse")
    @RequestWrapper(localName = "exportDataYaml", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataYaml")
    @ResponseWrapper(localName = "exportDataYamlResponse", targetNamespace = "http://endpoint.tm.volkov.jvschool.renessans.ru/", className = "ru.renessans.jvschool.volkov.tm.endpoint.ExportDataYamlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.renessans.jvschool.volkov.tm.endpoint.Domain exportDataYaml(
        @WebParam(name = "session", targetNamespace = "")
        ru.renessans.jvschool.volkov.tm.endpoint.Session session
    );
}
