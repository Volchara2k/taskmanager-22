package ru.renessans.jvschool.volkov.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.security.CommandAddFailureException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_ARGUMENTS = "arguments";

    @NotNull
    private static final String ARG_ARGUMENTS = "-arg";

    @NotNull
    private static final String DESC_ARGUMENTS = "вывод списка поддерживаемых программных аргументов";

    @NotNull
    private static final String NOTIFY_ARGUMENTS = "Список поддерживаемых программных аргументов: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_ARGUMENTS;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARG_ARGUMENTS;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_ARGUMENTS;
    }

    @SneakyThrows
    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_ARGUMENTS);
        @NotNull final ICommandService commandService = super.serviceLocator.getCommandService();
        @Nullable final Collection<AbstractCommand> arguments = commandService.getAllArgumentCommands();
        if (ValidRuleUtil.isNullOrEmpty(arguments)) throw new CommandAddFailureException("Программные аргументы");
        ViewUtil.print(arguments);
    }

}