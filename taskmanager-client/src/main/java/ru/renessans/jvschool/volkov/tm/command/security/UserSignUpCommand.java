package ru.renessans.jvschool.volkov.tm.command.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AuthenticationEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.User;
import ru.renessans.jvschool.volkov.tm.endpoint.UserRole;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserSignUpCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_SIGN_UP = "sign-up";

    @NotNull
    private static final String DESC_SIGN_UP = "зарегистрироваться в системе";

    @NotNull
    private static final String NOTIFY_SIGN_UP = "Для регистрации пользователя в системе введите логин и пароль: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_SIGN_UP;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_SIGN_UP;
    }

    @Nullable
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.UNKNOWN};
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_SIGN_UP);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final String password = ViewUtil.getLine();

        @NotNull final AuthenticationEndpoint authenticationEndpoint = super.serviceLocator.getAuthenticationEndpoint();
        @Nullable final User user = authenticationEndpoint.signUpUser(login, password);
        ViewUtil.print(user);
    }

}