package ru.renessans.jvschool.volkov.tm.command.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.UserRole;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserSignInCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_SIGN_IN = "sign-in";

    @NotNull
    private static final String DESC_SIGN_IN = "войти в систему";

    @NotNull
    private static final String NOTIFY_SIGN_IN = "Для авторизации пользователя в системе введите логин и пароль: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_SIGN_IN;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_SIGN_IN;
    }

    @Nullable
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.UNKNOWN};
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_SIGN_IN);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final String password = ViewUtil.getLine();

        @NotNull final SessionEndpoint sessionEndpoint = super.serviceLocator.getSessionEndpoint();
        @NotNull final Session opened = sessionEndpoint.openSession(login, password);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        currentSessionService.put(opened);
        ViewUtil.print(opened);
    }

}