package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Project;
import ru.renessans.jvschool.volkov.tm.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_LIST = "project-list";

    @NotNull
    private static final String DESC_PROJECT_LIST = "вывод списка проектов";

    @NotNull
    private static final String NOTIFY_PROJECT_LIST = "Текущий список проектов: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_LIST;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_LIST;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_PROJECT_LIST);
        @NotNull final ProjectEndpoint projectEndpoint = super.serviceLocator.getProjectEndpoint();
        @Nullable final Collection<Project> projects = projectEndpoint.getAllProjects(opened);
        ViewUtil.print(projects);
    }

}