package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IService;
import ru.renessans.jvschool.volkov.tm.endpoint.PermissionValidState;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.UserRole;

public interface ICurrentSessionService extends IService<Session> {

    @Nullable
    Session get();

    @NotNull
    Session put(@Nullable Session session);

    boolean delete();

    @NotNull
    PermissionValidState verifyPermissions(@Nullable UserRole[] commandRoles);

}