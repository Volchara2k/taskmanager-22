package ru.renessans.jvschool.volkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.User;
import ru.renessans.jvschool.volkov.tm.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class PasswordUpdateCommand extends AbstractAuthCommand {

    @NotNull
    private static final String CMD_UPDATE_PASSWORD = "update-password";

    @NotNull
    private static final String ARG_UPDATE_PASSWORD = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_UPDATE_PASSWORD = "Для смены пароля введите новый пароль: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_UPDATE_PASSWORD;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return ARG_UPDATE_PASSWORD;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_UPDATE_PASSWORD);
        @NotNull final ICurrentSessionService currentSession = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSession.get();
        @NotNull final String password = ViewUtil.getLine();

        @NotNull final UserEndpoint userEndpoint = super.serviceLocator.getUserEndpoint();
        @Nullable final User user = userEndpoint.updatePassword(opened, password);
        ViewUtil.print(user);
    }

}