package ru.renessans.jvschool.volkov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserDeleteCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_USER_DELETE = "user-delete";

    @NotNull
    private static final String DESC_USER_DELETE = "удалить пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_DELETE = "Для удаления пользователя в системе введите его логин. \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_USER_DELETE;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_USER_DELETE;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminEndpoint adminEndpoint = super.serviceLocator.getAdminEndpoint();

        ViewUtil.print(NOTIFY_USER_DELETE);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final User user = adminEndpoint.deleteUserByLogin(opened, login);
        ViewUtil.print(user);
    }

}