package ru.renessans.jvschool.volkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.Task;
import ru.renessans.jvschool.volkov.tm.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskDeleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_ID = "task-delete-by-id";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_ID = "удалить задачу по идентификатору";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_ID =
            "Для удаления задачи по идентификатору введите идентификатор задачи из списка.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_TASK_DELETE_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_TASK_DELETE_BY_ID;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_TASK_DELETE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();

        @NotNull final TaskEndpoint taskEndpoint = super.serviceLocator.getTaskEndpoint();
        @Nullable final Task task = taskEndpoint.deleteTaskById(opened, id);
        ViewUtil.print(task);
    }

}