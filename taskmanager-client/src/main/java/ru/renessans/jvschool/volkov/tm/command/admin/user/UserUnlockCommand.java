package ru.renessans.jvschool.volkov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.endpoint.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class UserUnlockCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_USER_UNLOCK = "user-unlock";

    @NotNull
    private static final String DESC_USER_UNLOCK = "разблокировать пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_UNLOCK = "Для разблокирования пользователя в системе введите его логин. \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_USER_UNLOCK;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_USER_UNLOCK;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminEndpoint adminEndpoint = super.serviceLocator.getAdminEndpoint();

        ViewUtil.print(NOTIFY_USER_UNLOCK);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final User user = adminEndpoint.unlockUserByLogin(opened, login);
        ViewUtil.print(user);
    }

}