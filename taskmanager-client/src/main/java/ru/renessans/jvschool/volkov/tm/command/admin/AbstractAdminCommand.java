package ru.renessans.jvschool.volkov.tm.command.admin;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.UserRole;

public abstract class AbstractAdminCommand extends AbstractCommand {

    @NotNull
    @Override
    public UserRole[] permissions() {
        return new UserRole[]{UserRole.ADMIN};
    }

}