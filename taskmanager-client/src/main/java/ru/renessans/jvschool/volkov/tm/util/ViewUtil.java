package ru.renessans.jvschool.volkov.tm.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.endpoint.AbstractModel;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyOwnerUserException;
import ru.renessans.jvschool.volkov.tm.exception.illegal.IllegalProcessCompleting;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@UtilityClass
public final class ViewUtil {

    @NotNull
    private static final String SUCCESSFUL_OUTCOME = "Операция УСПЕШНО завершилась!\n";

    @NotNull
    private static final String UNSUCCESSFUL_OUTCOME = "Операция завершилась с ОШИБКОЙ!\n";

    public void print(@NotNull final String string) {
        System.out.println(string);
    }

    @SneakyThrows
    public void print(final boolean state) {
        if (!state) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new IllegalProcessCompleting();
        }

        print(SUCCESSFUL_OUTCOME);
    }

    public void print(@Nullable final Collection<?> collection) {
        if (ValidRuleUtil.isNullOrEmpty(collection)) {
            print("Список на текущий момент пуст.");
            return;
        }

        @NotNull final AtomicInteger index = new AtomicInteger(1);
        collection.forEach(element -> {
            print(index + ". " + element);
            index.getAndIncrement();
        });

        print(SUCCESSFUL_OUTCOME);
    }

    @SneakyThrows
    public void print(@Nullable final AbstractModel model) {
        if (Objects.isNull(model)) {
            print(UNSUCCESSFUL_OUTCOME);
            throw new EmptyOwnerUserException();
        }

        print(model.toString());
    }

    @NotNull
    public String getLine() {
        System.out.print("Введите данные: ");
        return ScannerUtil.getLine();
    }

    @NotNull
    public Integer getInteger() {
        System.out.print("Введите данные: ");
        return ScannerUtil.getInteger();
    }

}