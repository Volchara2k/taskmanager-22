package ru.renessans.jvschool.volkov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.*;

public interface IServiceLocator {

    @NotNull
    AuthenticationEndpoint getAuthenticationEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    AdminDataInterChangeEndpoint getAdminInterChangeEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ICurrentSessionService getCurrentSession();

    @NotNull
    ICommandService getCommandService();

}