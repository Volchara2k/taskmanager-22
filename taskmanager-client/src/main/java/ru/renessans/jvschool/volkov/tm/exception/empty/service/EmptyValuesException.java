package ru.renessans.jvschool.volkov.tm.exception.empty.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyValuesException extends AbstractException {

    @NotNull
    private static final String EMPTY_VALUES = "Ошибка! Параметр \"значения\" является null!\n";

    public EmptyValuesException() {
        super(EMPTY_VALUES);
    }

}