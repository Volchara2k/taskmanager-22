package ru.renessans.jvschool.volkov.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class UnknownCommandException extends AbstractException {

    @NotNull
    private static final String UNKNOWN_COMMAND = "Ошибка! Неизвестная команда: %s!\n";

    public UnknownCommandException(@NotNull final String message) {
        super(String.format(UNKNOWN_COMMAND, message));
    }

}