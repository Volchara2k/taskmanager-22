package ru.renessans.jvschool.volkov.tm.command.admin.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Domain;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataBinaryImportCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_BIN_IMPORT = "data-bin-import";

    @NotNull
    private static final String DESC_BIN_IMPORT = "импортировать домен из бинарного вида";

    @NotNull
    private static final String NOTIFY_BIN_IMPORT = "Происходит процесс загрузки домена из бинарного вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_BIN_IMPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_BIN_IMPORT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_BIN_IMPORT);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminDataInterChangeEndpoint adminInterChangeEndpoint =
                super.serviceLocator.getAdminInterChangeEndpoint();
        @NotNull final Domain domain = adminInterChangeEndpoint.importDataBin(opened);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}