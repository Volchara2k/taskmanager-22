package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.api.IRepository;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;

public interface ICurrentSessionRepository extends IRepository<Session> {

    @NotNull
    Session getSession(@NotNull String sessionId);

}