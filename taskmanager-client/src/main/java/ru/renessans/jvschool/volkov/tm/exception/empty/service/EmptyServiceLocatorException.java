package ru.renessans.jvschool.volkov.tm.exception.empty.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyServiceLocatorException extends AbstractException {

    @NotNull
    private static final String EMPTY_LOCATOR = "Ошибка! Параметр \"служба поиска\" является null!\n";

    public EmptyServiceLocatorException() {
        super(EMPTY_LOCATOR);
    }

}