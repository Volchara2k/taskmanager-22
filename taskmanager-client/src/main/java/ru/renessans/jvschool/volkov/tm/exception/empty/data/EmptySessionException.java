package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptySessionException extends AbstractException {

    @NotNull
    private static final String EMPTY_SESSION =
            "Ошибка! Параметр \"сессия\" является null!\n";

    public EmptySessionException() {
        super(EMPTY_SESSION);
    }

}