package ru.renessans.jvschool.volkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.endpoint.Project;
import ru.renessans.jvschool.volkov.tm.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    @NotNull
    private static final String DESC_PROJECT_UPDATE_BY_INDEX = "обновить проект по индексу";

    @NotNull
    private static final String NOTIFY_PROJECT_UPDATE_BY_INDEX =
            "Для обновления проекта по индексу введите индекс проекта из списка.\n" +
                    "Для обновления проекта введите его заголовок или заголовок с описанием.\n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_UPDATE_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();

        ViewUtil.print(NOTIFY_PROJECT_UPDATE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @NotNull final ProjectEndpoint projectEndpoint = super.serviceLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.updateProjectByIndex(opened, index, title, description);
        ViewUtil.print(project);
    }

}