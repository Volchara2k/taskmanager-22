package ru.renessans.jvschool.volkov.tm.command;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.endpoint.UserRole;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getCommand();

    public abstract String getArgument();

    public abstract String getDescription();

    @Nullable
    public UserRole[] permissions() {
        return null;
    }

    public abstract void execute() throws Exception;

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        if (!ValidRuleUtil.isNullOrEmpty(getCommand()))
            result.append("Терминальная команда: ").append(getCommand());
        if (!ValidRuleUtil.isNullOrEmpty(getArgument()))
            result.append(", программный аргумент: ").append(getArgument());
        if (!ValidRuleUtil.isNullOrEmpty(getDescription()))
            result.append("\n\t - ").append(getDescription());
        return result.toString();
    }

}