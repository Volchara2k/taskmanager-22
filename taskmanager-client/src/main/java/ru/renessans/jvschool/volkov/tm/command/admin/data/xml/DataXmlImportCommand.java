package ru.renessans.jvschool.volkov.tm.command.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.tm.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.tm.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.tm.endpoint.Domain;
import ru.renessans.jvschool.volkov.tm.endpoint.Session;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DataXmlImportCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_XML_IMPORT = "data-xml-import";

    @NotNull
    private static final String DESC_XML_IMPORT = "импортировать домен из xml вида";

    @NotNull
    private static final String NOTIFY_XML_IMPORT = "Происходит процесс загрузки домена из xml вида...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_XML_IMPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_XML_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.print(NOTIFY_XML_IMPORT);
        @NotNull final ICurrentSessionService currentSessionService = super.serviceLocator.getCurrentSession();
        @Nullable final Session opened = currentSessionService.get();
        @NotNull final AdminDataInterChangeEndpoint adminInterChangeEndpoint =
                super.serviceLocator.getAdminInterChangeEndpoint();
        @NotNull final Domain domain = adminInterChangeEndpoint.importDataXml(opened);

        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}