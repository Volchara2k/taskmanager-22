package ru.renessans.jvschool.volkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.endpoint.AbstractModel;

import java.util.Collection;

public interface IService<E extends AbstractModel> {

    @NotNull
    E addRecord(@Nullable E value);

    @NotNull
    Collection<E> getAllRecords();

    @Nullable
    E updateRecord(@Nullable E value);

    @Nullable
    E getRecordByKey(@Nullable String key);

    @NotNull
    Collection<E> deleteAllRecords();

    @Nullable
    E deleteRecordByKey(@Nullable String key);

    @Nullable
    E deleteRecord(@Nullable E value);

    @NotNull
    Collection<E> setAllRecords(@Nullable Collection<E> values);

    boolean wasDeletedRecord(@Nullable E value);

    boolean wasDeletedAllRecords();

}