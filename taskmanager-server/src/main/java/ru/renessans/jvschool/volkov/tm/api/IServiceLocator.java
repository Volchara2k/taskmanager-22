package ru.renessans.jvschool.volkov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.api.service.*;

public interface IServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IAuthenticationService getAuthenticationService();

    @NotNull
    ITaskUserService getTaskService();

    @NotNull
    IProjectUserService getProjectService();

    @NotNull
    IDataInterChangeService getAdminDataInterChangeService();

}