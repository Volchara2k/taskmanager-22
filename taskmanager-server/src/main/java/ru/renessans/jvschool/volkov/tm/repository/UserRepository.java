package ru.renessans.jvschool.volkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;
import java.util.Objects;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User getByLogin(@NotNull final String login) {
        @NotNull final Collection<User> allData = super.getAllRecords();
        return allData
                .stream()
                .filter(user -> login.equals(user.getLogin()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User deleteByLogin(@NotNull final String login) {
        @Nullable final User user = getByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return super.deleteRecordByKey(user.getId());
    }

}