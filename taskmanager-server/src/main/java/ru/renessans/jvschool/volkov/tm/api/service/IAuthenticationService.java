package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthenticationService {

    @NotNull
    UserRole getUserRole(
            @Nullable String userId
    );

    @NotNull
    PermissionValidState verifyValidPermission(
            @Nullable String userId,
            @Nullable UserRole[] commandRoles
    );

    @NotNull
    AuthValidState verifyValidUserData(
            @Nullable final String login,
            @Nullable final String password
    );

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User signUp(
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRole role
    );

}