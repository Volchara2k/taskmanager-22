package ru.renessans.jvschool.volkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.endpoint.IProjectEndpoint;
import ru.renessans.jvschool.volkov.tm.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.tm.api.service.ISessionService;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Project addProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "title") @Nullable final String title,
            @WebParam(name = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.add(opened.getUserId(), title, description);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Project updateProjectByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "title") @Nullable final String title,
            @WebParam(name = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.updateByIndex(opened.getUserId(), index, title, description);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Project updateProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "title") @Nullable final String title,
            @WebParam(name = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.updateById(opened.getUserId(), id, title, description);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Project deleteProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.deleteById(opened.getUserId(), id);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Project deleteProjectByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.deleteByIndex(opened.getUserId(), index);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Project deleteProjectByTitle(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.deleteByTitle(opened.getUserId(), title);
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> deleteAllProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.deleteAll(opened.getUserId());
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Project getProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getById(opened.getUserId(), id);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Project getProjectByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getByIndex(opened.getUserId(), index);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Project getProjectByTitle(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getByTitle(opened.getUserId(), title);
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Project> getAllProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getAll(opened.getUserId());
    }

}