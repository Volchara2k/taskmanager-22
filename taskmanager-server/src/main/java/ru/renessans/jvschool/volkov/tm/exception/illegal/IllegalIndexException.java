package ru.renessans.jvschool.volkov.tm.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class IllegalIndexException extends AbstractException {

    @NotNull
    private static final String INDEX_NOT_INTEGER =
            "Ошибка! Значение \"%s\" параметра \"индекс\" не является целочисленным типом данных!\n";

    @NotNull
    private static final String INDEX_ILLEGAL = "Ошибка! Параметр \"индекс\" является нелегальным!\n";

    public IllegalIndexException() {
        super(INDEX_ILLEGAL);
    }

    public IllegalIndexException(@NotNull final String message) {
        super(String.format(INDEX_NOT_INTEGER, message));
    }

}