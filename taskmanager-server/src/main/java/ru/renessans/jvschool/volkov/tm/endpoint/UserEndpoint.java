package ru.renessans.jvschool.volkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.endpoint.IUserEndpoint;
import ru.renessans.jvschool.volkov.tm.api.service.ISessionService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.model.Session;
import ru.renessans.jvschool.volkov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(
            @NotNull final IServiceLocator service
    ) {
        super(service);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public User getUser(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.getRecordByKey(opened.getUserId());
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public User editProfile(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "firstName") @Nullable final String firstName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.editProfileById(opened.getId(), firstName);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public User editProfileWithLastName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.editProfileById(opened.getId(), firstName, lastName);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public User updatePassword(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "newPassword") @Nullable final String newPassword
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.updatePasswordById(opened.getId(), newPassword);
    }

}