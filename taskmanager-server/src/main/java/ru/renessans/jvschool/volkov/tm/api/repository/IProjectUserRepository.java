package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.model.Project;

public interface IProjectUserRepository extends IOwnerUserRepository<Project> {
}