package ru.renessans.jvschool.volkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

@UtilityClass
public final class ValidRuleUtil {

    public boolean isNullOrEmpty(@Nullable final String string) {
        return string == null || string.isEmpty();
    }

    public boolean isNullOrEmpty(@Nullable final Integer aInteger) {
        return aInteger == null || aInteger < 0;
    }

    public boolean isNullOrEmpty(@Nullable final Long aLong) {
        return aLong == null || aLong < 0;
    }

    public boolean isNullOrEmpty(@Nullable final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

}