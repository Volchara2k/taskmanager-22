package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyUserRoleException extends AbstractException {

    @NotNull
    private static final String EMPTY_USER_ROLE = "Ошибка! Параметр \"тип пользователя\" является null!\n";

    public EmptyUserRoleException() {
        super(EMPTY_USER_ROLE);
    }

}