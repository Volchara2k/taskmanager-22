package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    void loadProperties();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

    @NotNull
    String getBinFileName();

    @NotNull
    String getBase64FileName();

    @NotNull
    String getJsonFileName();

    @NotNull
    String getXmlFileName();

    @NotNull
    String getYamlFileName();

}