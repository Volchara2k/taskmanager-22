package ru.renessans.jvschool.volkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.endpoint.IAdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.tm.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.tm.api.service.ISessionService;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminDataInterChangeEndpoint extends AbstractEndpoint implements IAdminDataInterChangeEndpoint {

    @NotNull
    private static final UserRole[] ROLES = new UserRole[]{UserRole.ADMIN};

    public AdminDataInterChangeEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public boolean dataBinClear(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.dataBinClear();
    }

    @WebMethod
    @Override
    public boolean dataBase64Clear(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.dataBase64Clear();
    }

    @WebMethod
    @Override
    public boolean dataJsonClear(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.dataJsonClear();
    }

    @WebMethod
    @Override
    public boolean dataXmlClear(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.dataXmlClear();
    }

    @WebMethod
    @Override
    public boolean dataYamlClear(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.dataYamlClear();
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataBin(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.exportDataBin();
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataBase64(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.exportDataBase64();
    }

    @WebMethod
    @NotNull
    @Override
    public Domain exportDataJson(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.exportDataJson();
    }

    @WebMethod
    @NotNull
    @Override
    public Domain exportDataXml(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.exportDataXml();
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataYaml(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.exportDataYaml();
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Domain importDataBin(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.importDataBin();
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Domain importDataBase64(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.importDataBase64();
    }

    @WebMethod
    @Nullable
    @Override
    public Domain importDataJson(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.importDataJson();
    }

    @WebMethod
    @Nullable
    @Override
    public Domain importDataXml(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.importDataXml();
    }

    @WebMethod
    @Nullable
    @Override
    public Domain importDataYaml(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull IDataInterChangeService interChangeService = super.serviceLocator.getAdminDataInterChangeService();
        return interChangeService.importDataYaml();
    }

}