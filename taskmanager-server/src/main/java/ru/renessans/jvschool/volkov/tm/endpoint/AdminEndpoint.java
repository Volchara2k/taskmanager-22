package ru.renessans.jvschool.volkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.endpoint.IAdminEndpoint;
import ru.renessans.jvschool.volkov.tm.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.tm.api.service.ISessionService;
import ru.renessans.jvschool.volkov.tm.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Session;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    private static final UserRole[] ROLES = new UserRole[]{UserRole.ADMIN};

    public AdminEndpoint(
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public boolean closeAllSessions(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        return sessionService.closeAllSessions(session);
    }

    @WebMethod
    @NotNull
    @Override
    public Collection<Session> getAllSessions(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        return sessionService.getAllRecords();
    }

    @WebMethod
    @NotNull
    @Override
    public User signUpUserWithUserRole(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "userRole") @Nullable final UserRole role
    ) {
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.addUser(login, password, role);
    }

    @WebMethod
    @Nullable
    public User deleteUserById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.deleteRecordByKey(id);
    }

    @WebMethod
    @Nullable
    public User deleteUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.deleteUserByLogin(login);
    }

    @WebMethod
    @NotNull
    public Collection<User> deleteAllUsers(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.deleteAllRecords();
    }

    @WebMethod
    @Nullable
    public User lockUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.lockUserByLogin(login);
    }

    @WebMethod
    @Nullable
    public User unlockUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.unlockUserByLogin(login);
    }

    @WebMethod
    @NotNull
    public Collection<User> setAllUsers(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "users") @Nullable final Collection<User> values
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.setAllRecords(values);
    }

    @WebMethod
    @NotNull
    public Collection<User> getAllUsers(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.getAllRecords();
    }

    @WebMethod
    @SneakyThrows
    @NotNull
    public Collection<Task> getAllUsersTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getAllRecords();
    }

    @WebMethod
    @SneakyThrows
    @NotNull
    public Collection<Task> setAllUsersTasks(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "tasks") @Nullable final Collection<Task> values
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.setAllRecords(values);
    }

    @WebMethod
    @SneakyThrows
    @NotNull
    public Collection<Project> getAllUsersProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.getAllRecords();
    }

    @WebMethod
    @SneakyThrows
    @NotNull
    public Collection<Project> setAllUsersProjects(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "tasks") @Nullable final Collection<Project> values
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IProjectUserService projectService = super.serviceLocator.getProjectService();
        return projectService.setAllRecords(values);
    }

    @WebMethod
    @Nullable
    public User getUserById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.getRecordByKey(id);
    }

    @WebMethod
    @Nullable
    public User getUserByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.getUserByLogin(login);
    }

    @WebMethod
    @Nullable
    public User editProfileById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "firstName") @Nullable final String firstName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.editProfileById(id, firstName);
    }

    @WebMethod
    @Nullable
    public User editProfileByIdWithLastName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.editProfileById(id, firstName, lastName);
    }

    @WebMethod
    @Nullable
    public User updatePasswordById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "newPassword") @Nullable final String newPassword
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        sessionService.validateSession(session, ROLES);
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.updatePasswordById(id, newPassword);
    }

}