package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.dto.Domain;

public interface IDomainService {

    void dataImport(@Nullable Domain domain);

    void dataExport(@Nullable Domain domain);

}