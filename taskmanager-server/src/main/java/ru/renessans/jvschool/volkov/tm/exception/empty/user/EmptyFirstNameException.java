package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyFirstNameException extends AbstractException {

    @NotNull
    private static final String EMPTY_FIRST_NAME = "Ошибка! Параметр \"имя\" является пустым или null!\n";

    public EmptyFirstNameException() {
        super(EMPTY_FIRST_NAME);
    }

}