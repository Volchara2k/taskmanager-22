package ru.renessans.jvschool.volkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.endpoint.IAuthenticationEndpoint;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.Session;
import ru.renessans.jvschool.volkov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Objects;

@WebService
public final class AuthenticationEndpoint extends AbstractEndpoint implements IAuthenticationEndpoint {

    public AuthenticationEndpoint(
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @NotNull
    @Override
    public User signUpUser(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.addUser(login, password);
    }

    @WebMethod
    @NotNull
    @Override
    public User signUpUserWithFirstName(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "firstName") @Nullable final String firstName
    ) {
        @NotNull final IUserService userService = super.serviceLocator.getUserService();
        return userService.addUser(login, password, firstName);
    }

    @WebMethod
    @NotNull
    @Override
    public UserRole getUserRole(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @Nullable final IAuthenticationService authService = super.serviceLocator.getAuthenticationService();
        @Nullable String userId = null;
        if (!Objects.isNull(session)) {
            userId = session.getUserId();
        }
        return authService.getUserRole(userId);
    }

}