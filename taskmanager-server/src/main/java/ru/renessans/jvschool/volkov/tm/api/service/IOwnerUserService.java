package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IService;
import ru.renessans.jvschool.volkov.tm.model.AbstractModel;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IOwnerUserService<E extends AbstractModel> extends IService<E> {

    @NotNull
    E add(
            @Nullable String userId,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    E updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    E updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    E deleteByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    E deleteById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    E deleteByTitle(
            @Nullable String userId,
            @Nullable String title
    );

    @NotNull
    Collection<E> deleteAll(
            @Nullable String userId
    );

    @Nullable
    E getByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @Nullable
    E getById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    E getByTitle(
            @Nullable String userId,
            @Nullable String title
    );

    @NotNull
    Collection<E> getAll(
            @Nullable String userId
    );

    @NotNull
    Collection<E> initDemoData(
            @Nullable Collection<User> users
    );

}