package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.model.Task;

public interface ITaskUserRepository extends IOwnerUserRepository<Task> {
}