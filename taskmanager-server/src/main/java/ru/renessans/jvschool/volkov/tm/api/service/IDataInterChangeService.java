package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.dto.Domain;

public interface IDataInterChangeService {

    boolean dataBinClear();

    boolean dataBase64Clear();

    boolean dataJsonClear();

    boolean dataXmlClear();

    boolean dataYamlClear();

    @NotNull
    Domain exportDataBin();

    @NotNull
    Domain exportDataBase64();

    @NotNull
    Domain exportDataJson();

    @NotNull
    Domain exportDataXml();

    @NotNull
    Domain exportDataYaml();

    @Nullable
    Domain importDataBin();

    @Nullable
    Domain importDataBase64();

    @Nullable
    Domain importDataJson();

    @Nullable
    Domain importDataXml();

    @Nullable
    Domain importDataYaml();

}