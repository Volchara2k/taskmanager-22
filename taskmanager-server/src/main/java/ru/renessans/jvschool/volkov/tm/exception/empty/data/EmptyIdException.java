package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyIdException extends AbstractException {

    @NotNull
    private static final String EMPTY_ID = "Ошибка! Параметр \"идентификатор\" является пустым или null!\n";

    public EmptyIdException() {
        super(EMPTY_ID);
    }

}