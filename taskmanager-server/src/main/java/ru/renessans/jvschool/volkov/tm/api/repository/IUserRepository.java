package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IRepository;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User getByLogin(@NotNull String login);

    @Nullable
    User deleteByLogin(@NotNull String login);

}