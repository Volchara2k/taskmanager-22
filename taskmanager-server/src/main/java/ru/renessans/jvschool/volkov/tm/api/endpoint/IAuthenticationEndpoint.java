package ru.renessans.jvschool.volkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.Session;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthenticationEndpoint {

    @NotNull
    User signUpUser(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User signUpUserWithFirstName(
            @Nullable String login,
            @Nullable String password,
            @Nullable String firstName
    );

    @NotNull
    UserRole getUserRole(
            @Nullable Session session
    );

}