package ru.renessans.jvschool.volkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.model.Session;
import ru.renessans.jvschool.volkov.tm.model.Task;

import java.util.Collection;

public interface ITaskEndpoint {

    @NotNull
    Task addTask(
            @Nullable Session session,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    Task updateTaskById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    Task updateTaskByIndex(
            @Nullable Session session,
            @Nullable Integer index,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    Task deleteTaskById(
            @Nullable Session session,
            @Nullable String id
    );

    @Nullable
    Task deleteTaskByIndex(
            @Nullable Session session,
            @Nullable Integer index
    );

    @Nullable
    Task deleteTaskByTitle(
            @Nullable Session session,
            @Nullable String title
    );

    @NotNull
    Collection<Task> deleteAllTasks(
            @Nullable Session session
    );

    @Nullable
    Task getTaskById(
            @Nullable Session session,
            @Nullable String id
    );

    @Nullable
    Task getTaskByIndex(
            @Nullable Session session,
            @Nullable Integer index
    );

    @Nullable
    Task getTaskByTitle(
            @Nullable Session session,
            @Nullable String title
    );

    @NotNull
    Collection<Task> getAllTasks(
            @Nullable Session session
    );

}