package ru.renessans.jvschool.volkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IService;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.Session;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface ISessionService extends IService<Session> {

    @NotNull
    Session setSignature(
            @Nullable Session session
    );

    @NotNull
    Session openSession(
            @Nullable String login,
            @Nullable String password
    );

    boolean closeSession(
            @Nullable Session session
    );

    boolean closeAllSessions(
            @Nullable Session session
    );

    @NotNull
    Collection<Session> getSessionByUserId(
            @Nullable Session session
    );

    void deleteSessionByUserId(
            @Nullable Session session
    );

    @NotNull
    User validateUserData(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    AuthValidState verifyValidUserData(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    Session validateSession(
            @Nullable Session session
    );

    @NotNull
    Session validateSession(
            @Nullable Session session,
            @Nullable UserRole[] commandRoles
    );

    @NotNull
    SessionValidState verifyValidSessionState(
            @Nullable Session session
    );

    @NotNull
    PermissionValidState verifyValidPermissionState(
            @Nullable Session session,
            @Nullable UserRole[] commandRoles
    );

}