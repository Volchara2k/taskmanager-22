package ru.renessans.jvschool.volkov.tm.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class IllegalHashAlgorithmException extends AbstractException {

    @NotNull
    private static final String HASH_ALGORITHM_ILLEGAL = "Ошибка! Параметр \"хеш строки\" является нелегальным!\n";

    public IllegalHashAlgorithmException() {
        super(HASH_ALGORITHM_ILLEGAL);
    }

}