package ru.renessans.jvschool.volkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String PROPERTY_SOURCE = "/application.properties";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SESSION_CYCLE_KEY = "session.cycle";

    @NotNull
    private static final String SESSION_SALT_KEY = "session.salt";

    @NotNull
    private static final String FILE_NAME_BIN = "file.name.bin";

    @NotNull
    private static final String FILE_NAME_BASE64 = "file.name.base64";

    @NotNull
    private static final String FILE_NAME_JSON = "file.name.json";

    @NotNull
    private static final String FILE_NAME_XML = "file.name.xml";

    @NotNull
    private static final String FILE_NAME_YAML = "file.name.yaml";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    @Override
    public void loadProperties() {
        @NotNull final InputStream inputStream = PropertyService.class.getResourceAsStream(PROPERTY_SOURCE);
        this.properties.load(inputStream);
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = this.properties.getProperty(SERVER_HOST_KEY);
        @NotNull final String environmentHost = System.getProperty(SERVER_HOST_KEY);
        if (!Objects.isNull(environmentHost)) return environmentHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String propertyPort = this.properties.getProperty(SERVER_PORT_KEY);
        @NotNull final String environmentPort = System.getProperty(SERVER_PORT_KEY);
        if (!Objects.isNull(environmentPort)) return Integer.parseInt(environmentPort);
        return Integer.parseInt(propertyPort);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return this.properties.getProperty(SESSION_SALT_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        @NotNull final String cycle = this.properties.getProperty(SESSION_CYCLE_KEY);
        return Integer.parseInt(cycle);
    }

    @NotNull
    @Override
    public String getBinFileName() {
        return this.properties.getProperty(FILE_NAME_BIN);
    }

    @NotNull
    @Override
    public String getBase64FileName() {
        return this.properties.getProperty(FILE_NAME_BASE64);
    }

    @NotNull
    @Override
    public String getJsonFileName() {
        return this.properties.getProperty(FILE_NAME_JSON);
    }

    @NotNull
    @Override
    public String getXmlFileName() {
        return this.properties.getProperty(FILE_NAME_XML);
    }

    @NotNull
    @Override
    public String getYamlFileName() {
        return this.properties.getProperty(FILE_NAME_YAML);
    }

}