package ru.renessans.jvschool.volkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class ProjectUserRepository extends AbstractRepository<Project> implements IProjectUserRepository {

    @Nullable
    @SneakyThrows
    @Override
    public Project deleteByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return super.deleteRecordByKey(project.getId());
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project deleteById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final Project project = getById(userId, id);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return super.deleteRecordByKey(project.getId());
    }

    @Nullable
    @SneakyThrows
    @Override
    public Project deleteByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        @Nullable final Project project = getByTitle(userId, title);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return super.deleteRecordByKey(project.getId());
    }

    @NotNull
    @Override
    public Collection<Project> deleteAll(
            @NotNull final String userId
    ) {
        @Nullable final Collection<Project> removedProjects = getAll(userId);
        super.recordMap.entrySet().removeIf(taskEntry ->
                userId.equals(taskEntry.getValue().getUserId()));
        return removedProjects;
    }

    @NotNull
    @Override
    public Collection<Project> getAll(
            @NotNull final String userId
    ) {
        @NotNull final Collection<Project> allData = super.getAllRecords();
        return allData
                .stream()
                .filter(project -> userId.equals(project.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Project getByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @NotNull final List<Project> userProjects = new ArrayList<>(getAll(userId));
        return userProjects.get(index);
    }

    @Nullable
    @Override
    public Project getById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> id.equals(task.getId()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project getByTitle(
            @NotNull final String userId,
            @NotNull final String title
    ) {
        return getAll(userId)
                .stream()
                .filter(task -> title.equals(task.getTitle()))
                .findAny()
                .orElse(null);
    }

}