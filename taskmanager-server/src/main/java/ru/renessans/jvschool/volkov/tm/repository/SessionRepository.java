package ru.renessans.jvschool.volkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.api.repository.ISessionRepository;
import ru.renessans.jvschool.volkov.tm.model.Session;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean containsUserId(@NotNull final String userId) {
        @NotNull final Collection<Session> allSessions = super.getAllRecords();
        @NotNull final AtomicBoolean isContains = new AtomicBoolean(false);
        allSessions.forEach(session -> {
            if (userId.equals(session.getUserId())) isContains.set(true);
        });
        return isContains.get();
    }

    @NotNull
    @Override
    public Collection<Session> getSessionByUserId(@NotNull final String userId) {
        @NotNull final Collection<Session> sessions = super.getAllRecords();
        return sessions
                .stream()
                .filter(session -> userId.equals(session.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteByUserId(@NotNull final String userId) {
        @NotNull final Collection<Session> userSessions = getSessionByUserId(userId);
        userSessions.forEach(super::deleteRecord);
    }

}