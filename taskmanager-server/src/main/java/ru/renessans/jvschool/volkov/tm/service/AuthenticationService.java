package ru.renessans.jvschool.volkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyEmailException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyLoginException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyPasswordException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserRoleException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.HashUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Arrays;
import java.util.Objects;

public final class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;

    public AuthenticationService(
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public UserRole getUserRole(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) return UserRole.UNKNOWN;
        @Nullable final User user = this.userService.getRecordByKey(userId);
        if (Objects.isNull(user)) return UserRole.UNKNOWN;
        return user.getRole();
    }

    @NotNull
    @SneakyThrows
    @Override
    public PermissionValidState verifyValidPermission(
            @Nullable final String userId,
            @Nullable final UserRole[] commandRoles
    ) {
        if (Objects.isNull(commandRoles)) return PermissionValidState.NEED_COMMAND_ROLE;

        @NotNull final UserRole userRole = getUserRole(userId);
        boolean containsTypes = Arrays.asList(commandRoles).contains(userRole);
        if (containsTypes) return PermissionValidState.SUCCESS;

        if (userRole.isUser()) return PermissionValidState.NO_ACCESS_RIGHTS;
        else return PermissionValidState.NEED_LOG_IN;
    }

    @NotNull
    @SneakyThrows
    @Override
    public AuthValidState verifyValidUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) return AuthValidState.USER_NOT_FOUND;
        if (user.getLockdown()) return AuthValidState.LOCKDOWN_PROFILE;

        @NotNull final String passwordHash = HashUtil.saltHashLine(password);
        if (!passwordHash.equals(user.getPasswordHash())) return AuthValidState.INVALID_PASSWORD;

        return AuthValidState.SUCCESS;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        return this.userService.addUser(login, password);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(email)) throw new EmptyEmailException();
        return this.userService.addUser(login, password, email);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole role
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(role)) throw new EmptyUserRoleException();
        return this.userService.addUser(login, password, role);
    }

}