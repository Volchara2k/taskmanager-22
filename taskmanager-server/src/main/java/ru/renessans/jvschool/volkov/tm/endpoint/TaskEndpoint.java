package ru.renessans.jvschool.volkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.endpoint.ITaskEndpoint;
import ru.renessans.jvschool.volkov.tm.api.service.ISessionService;
import ru.renessans.jvschool.volkov.tm.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.tm.model.Session;
import ru.renessans.jvschool.volkov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Task addTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "title") @Nullable final String title,
            @WebParam(name = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.add(opened.getUserId(), title, description);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Task updateTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "title") @Nullable final String title,
            @WebParam(name = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.updateById(opened.getUserId(), id, title, description);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Task updateTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "title") @Nullable final String title,
            @WebParam(name = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.updateByIndex(opened.getUserId(), index, title, description);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Task deleteTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.deleteById(opened.getUserId(), id);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Task deleteTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.deleteByIndex(opened.getUserId(), index);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Task deleteTaskByTitle(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.deleteByTitle(opened.getUserId(), title);
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> deleteAllTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.deleteAll(opened.getUserId());
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Task getTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getById(opened.getUserId(), id);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Task getTaskByIndex(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getByIndex(opened.getUserId(), index);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    @Override
    public Task getTaskByTitle(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getByTitle(opened.getUserId(), title);
    }

    @WebMethod
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> getAllTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getAll(opened.getUserId());
    }

}