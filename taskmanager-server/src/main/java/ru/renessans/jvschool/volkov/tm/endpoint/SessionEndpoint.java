package ru.renessans.jvschool.volkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.endpoint.ISessionEndpoint;
import ru.renessans.jvschool.volkov.tm.api.service.ISessionService;
import ru.renessans.jvschool.volkov.tm.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @NotNull
    @Override
    public Session openSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        return sessionService.openSession(login, password);
    }

    @WebMethod
    @Override
    public boolean closeSession(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        return sessionService.closeSession(session);
    }

    @WebMethod
    @Nullable
    @Override
    public Session getUserSession(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        return sessionService.getRecordByKey(opened.getUserId());
    }

    @WebMethod
    @NotNull
    @Override
    public Session validateSession(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        return opened;
    }

    @WebMethod
    @NotNull
    @Override
    public Session validateSessionWithCommandRole(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "roles") @Nullable final UserRole[] commandRoles
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session, commandRoles);
        return opened;
    }

    @NotNull
    @Override
    public SessionValidState verifyValidSessionState(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        return sessionService.verifyValidSessionState(opened);
    }

    @NotNull
    @Override
    public PermissionValidState verifyValidPermissionState(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "roles") @Nullable final UserRole[] commandRoles
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session, commandRoles);
        return sessionService.verifyValidPermissionState(opened, commandRoles);
    }

}