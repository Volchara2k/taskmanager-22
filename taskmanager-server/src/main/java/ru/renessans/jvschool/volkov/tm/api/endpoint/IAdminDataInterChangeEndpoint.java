package ru.renessans.jvschool.volkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.model.Session;

public interface IAdminDataInterChangeEndpoint {

    boolean dataBinClear(@Nullable final Session session);

    boolean dataBase64Clear(@Nullable final Session session);

    boolean dataJsonClear(@Nullable final Session session);

    boolean dataXmlClear(@Nullable final Session session);

    boolean dataYamlClear(@Nullable final Session session);

    @NotNull
    Domain exportDataBin(@Nullable final Session session);

    @NotNull
    Domain exportDataBase64(@Nullable final Session session);

    @NotNull
    Domain exportDataJson(@Nullable final Session session);

    @NotNull
    Domain exportDataXml(@Nullable final Session session);

    @NotNull
    Domain exportDataYaml(@Nullable final Session session);

    @Nullable
    Domain importDataBin(@Nullable final Session session);

    @Nullable
    Domain importDataBase64(@Nullable final Session session);

    @Nullable
    Domain importDataJson(@Nullable final Session session);

    @Nullable
    Domain importDataXml(@Nullable final Session session);

    @Nullable
    Domain importDataYaml(@Nullable final Session session);

}