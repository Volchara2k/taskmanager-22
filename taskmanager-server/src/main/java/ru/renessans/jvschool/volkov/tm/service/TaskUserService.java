package ru.renessans.jvschool.volkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.tm.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyDescriptionException;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyIdException;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyTitleException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.EmptyUserIdException;
import ru.renessans.jvschool.volkov.tm.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class TaskUserService extends AbstractService<Task> implements ITaskUserService {

    @NotNull
    private final ITaskUserRepository taskRepository;

    public TaskUserService(
            @NotNull final ITaskUserRepository repository
    ) {
        super(repository);
        this.taskRepository = repository;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task add(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @NotNull final Task task = new Task(title, description);
        task.setUserId(userId);
        return super.addRecord(task);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Task task = getByIndex(userId, index);
        if (Objects.isNull(task)) throw new EmptyTaskException();

        task.setTitle(title);
        task.setDescription(description);
        return super.updateRecord(task);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        @Nullable final Task task = getById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();

        task.setTitle(title);
        task.setDescription(description);
        return super.updateRecord(task);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task deleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.taskRepository.deleteByIndex(userId, index);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task deleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.taskRepository.deleteById(userId, id);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task deleteByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.taskRepository.deleteByTitle(userId, title);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> deleteAll(@Nullable final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.taskRepository.deleteAll(userId);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task getByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        return this.taskRepository.getByIndex(userId, index);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task getById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.taskRepository.getById(userId, id);
    }

    @Nullable
    @SneakyThrows
    @Override
    public Task getByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.taskRepository.getByTitle(userId, title);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> getAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.taskRepository.getAll(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> initDemoData(
            @Nullable final Collection<User> users
    ) {
        if (Objects.isNull(users)) throw new EmptyUserException();

        @NotNull final List<Task> assignedData = new ArrayList<>();
        users.forEach(user -> {
            @NotNull final Task task =
                    add(user.getId(), DemoDataConst.TASK_TITLE, DemoDataConst.TASK_DESCRIPTION);
            assignedData.add(task);
        });

        return assignedData;
    }

}