package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyDomainException extends AbstractException {

    @NotNull
    private static final String EMPTY_DOMAIN = "Ошибка! Параметр \"домен\" является null!\n";

    public EmptyDomainException() {
        super(EMPTY_DOMAIN);
    }

}