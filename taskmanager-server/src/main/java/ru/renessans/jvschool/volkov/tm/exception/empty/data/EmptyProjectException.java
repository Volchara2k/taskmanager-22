package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyProjectException extends AbstractException {

    @NotNull
    private static final String EMPTY_PROJECT = "Ошибка! Параметр \"проект\" является null!\n";

    public EmptyProjectException() {
        super(EMPTY_PROJECT);
    }

}