package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.Task;

public interface ITaskUserService extends IOwnerUserService<Task> {
}