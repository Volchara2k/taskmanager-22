package ru.renessans.jvschool.volkov.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyFileException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public final class DataSerializerUtil implements Closeable {

    @Nullable
    private ObjectInputStream objectInputStream;

    @Nullable
    private ObjectOutputStream objectOutputStream;

    @Nullable
    private ByteArrayInputStream byteArrayInputStream;

    @Nullable
    private ByteArrayOutputStream byteArrayOutputStream;

    @Nullable
    private FileInputStream fileInputStream;

    @Nullable
    private FileOutputStream fileOutputStream;

    @Override
    public void close() throws IOException {
        if (!Objects.isNull(this.objectOutputStream)) this.objectOutputStream.close();
        if (!Objects.isNull(this.objectInputStream)) this.objectInputStream.close();
        if (!Objects.isNull(this.byteArrayOutputStream)) this.byteArrayOutputStream.close();
        if (!Objects.isNull(this.byteArrayInputStream)) this.byteArrayInputStream.close();
        if (!Objects.isNull(this.fileOutputStream)) this.fileOutputStream.close();
        if (!Objects.isNull(this.fileInputStream)) this.fileInputStream.close();
    }

    @SneakyThrows
    public <T extends Serializable> void writeToBin(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        @NotNull final File file = FileUtil.createEmptyFile(filename);
        this.fileOutputStream = new FileOutputStream(file);
        writeAsFile(t, fileOutputStream);
    }

    @Nullable
    @SneakyThrows
    public <T extends Serializable> T readFromBin(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();
        this.fileInputStream = new FileInputStream(filename);
        return readObject(fileInputStream, tClass);
    }

    @SneakyThrows
    public <T extends Serializable> void writeToBase64(
            @NotNull final T t,
            @NotNull final String filename
    ) {
        this.byteArrayOutputStream = new ByteArrayOutputStream();
        writeAsFile(t, byteArrayOutputStream);

        final byte[] fileBytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(fileBytes);
        final byte[] base64Bytes = base64.getBytes(StandardCharsets.UTF_8);

        writeAsFile(base64Bytes, filename);
    }

    @Nullable
    @SneakyThrows
    public <T extends Serializable> T readFromBase64(
            @NotNull final String filename,
            @NotNull final Class<T> tClass
    ) {
        if (FileUtil.fileIsNotExists(filename)) throw new EmptyFileException();

        final byte[] fileBytes = FileUtil.readFile(filename);
        @NotNull final String base64 = new String(fileBytes);
        final byte[] base64Bytes = new BASE64Decoder().decodeBuffer(base64);

        this.byteArrayInputStream = new ByteArrayInputStream(base64Bytes);
        return readObject(this.byteArrayInputStream, tClass);
    }

    @SneakyThrows
    private <T extends Serializable> T readObject(
            @NotNull final InputStream inputStream,
            @NotNull final Class<T> tClass
    ) {
        this.objectInputStream = new ObjectInputStream(inputStream);
        return tClass.cast(this.objectInputStream.readObject());
    }

    @SneakyThrows
    private <T extends Serializable> void writeAsFile(
            @NotNull final T t,
            @NotNull final OutputStream outputStream
    ) {
        this.objectOutputStream = new ObjectOutputStream(outputStream);
        this.objectOutputStream.writeObject(t);
        this.objectOutputStream.flush();
    }

    @SneakyThrows
    private void writeAsFile(
            final byte[] bytes,
            @NotNull final String filename
    ) {
        @NotNull final File file = FileUtil.createEmptyFile(filename);
        this.fileOutputStream = new FileOutputStream(file.getName());
        this.fileOutputStream.write(bytes);
        this.fileOutputStream.flush();
    }

}