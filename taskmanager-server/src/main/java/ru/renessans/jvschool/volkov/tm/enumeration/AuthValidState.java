package ru.renessans.jvschool.volkov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum AuthValidState {

    @NotNull
    SUCCESS("Успешно!"),

    @NotNull
    USER_NOT_FOUND("Пользователь не найден!"),

    @NotNull
    INVALID_PASSWORD("Некорректный пароль!"),

    @NotNull
    LOCKDOWN_PROFILE("Профиль заблокирован!");

    @NotNull
    private final String title;

    AuthValidState(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

    public boolean isNotSuccess() {
        return this != SUCCESS;
    }

}