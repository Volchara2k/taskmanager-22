package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.model.Project;

public interface IProjectUserService extends IOwnerUserService<Project> {
}