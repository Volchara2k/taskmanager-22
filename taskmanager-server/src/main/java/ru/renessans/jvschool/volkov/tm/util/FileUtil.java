package ru.renessans.jvschool.volkov.tm.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@UtilityClass
public final class FileUtil {

    @SneakyThrows
    @NotNull
    public File createEmptyFile(@NotNull final String filename) {
        @NotNull final File file = new File(filename);
        deleteFile(filename);
        Files.createFile(file.toPath());
        return file;
    }

    @SneakyThrows
    public byte[] readFile(@NotNull final String filename) {
        return Files.readAllBytes(Paths.get(filename));
    }

    @SneakyThrows
    public boolean deleteFile(@NotNull final String filename) {
        if (fileIsNotExists(filename)) return false;
        @NotNull final Path path = Paths.get(filename);
        return Files.deleteIfExists(path);
    }

    public boolean fileIsNotExists(@NotNull final File file) {
        return !file.exists() || file.isDirectory();
    }

    public boolean fileIsNotExists(@NotNull final String fileName) {
        @NotNull final File file = new File(fileName);
        return fileIsNotExists(file);
    }

}