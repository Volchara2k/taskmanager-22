package ru.renessans.jvschool.volkov.tm.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.api.service.IPropertyService;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.util.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.tm.util.DataSerializerUtil;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;

@AllArgsConstructor
public final class DataInterChangeService implements IDataInterChangeService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IDomainService domainService;

    @Override
    public boolean dataBinClear() {
        @NotNull final String locate = this.propertyService.getBinFileName();
        return FileUtil.deleteFile(locate);
    }

    @Override
    public boolean dataBase64Clear() {
        @NotNull final String locate = this.propertyService.getBase64FileName();
        return FileUtil.deleteFile(locate);
    }

    @Override
    public boolean dataJsonClear() {
        @NotNull final String locate = this.propertyService.getJsonFileName();
        return FileUtil.deleteFile(locate);
    }

    @Override
    public boolean dataXmlClear() {
        @NotNull final String locate = this.propertyService.getXmlFileName();
        return FileUtil.deleteFile(locate);
    }

    @Override
    public boolean dataYamlClear() {
        @NotNull final String locate = this.propertyService.getYamlFileName();
        return FileUtil.deleteFile(locate);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataBin() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.propertyService.getBinFileName();
        try (@Nullable final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            dataInterchange.writeToBin(domain, locate);
        }

        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataBase64() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.propertyService.getBase64FileName();
        try (@Nullable final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            dataInterchange.writeToBase64(domain, locate);
        }

        return domain;
    }

    @NotNull
    @Override
    public Domain exportDataJson() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.propertyService.getJsonFileName();
        DataMarshalizerUtil.writeToJson(domain, locate);

        return domain;
    }

    @NotNull
    @Override
    public Domain exportDataXml() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.propertyService.getXmlFileName();
        DataMarshalizerUtil.writeToXml(domain, locate);

        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataYaml() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.propertyService.getXmlFileName();
        DataMarshalizerUtil.writeToYaml(domain, locate);

        return domain;
    }

    @Nullable
    @SneakyThrows
    @Override
    public Domain importDataBin() {
        @NotNull final String locate = this.propertyService.getBinFileName();
        @Nullable final Domain domain;

        try (@NotNull final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            domain = dataInterchange.readFromBin(locate, Domain.class);
        }

        this.domainService.dataImport(domain);
        return domain;
    }

    @Nullable
    @SneakyThrows
    @Override
    public Domain importDataBase64() {
        @NotNull final String locate = this.propertyService.getBase64FileName();
        @Nullable final Domain domain;

        try (@NotNull final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            domain = dataInterchange.readFromBase64(locate, Domain.class);
        }

        this.domainService.dataImport(domain);
        return domain;
    }

    @Nullable
    @Override
    public Domain importDataJson() {
        @NotNull final String locate = this.propertyService.getJsonFileName();
        @Nullable final Domain domain;

        domain = DataMarshalizerUtil.readFromJson(locate, Domain.class);
        this.domainService.dataImport(domain);

        return domain;
    }

    @Nullable
    @Override
    public Domain importDataXml() {
        @NotNull final String locate = this.propertyService.getXmlFileName();
        @Nullable final Domain domain;

        domain = DataMarshalizerUtil.readFromXml(locate, Domain.class);
        this.domainService.dataImport(domain);

        return domain;
    }

    @Nullable
    @Override
    public Domain importDataYaml() {
        @NotNull final String locate = this.propertyService.getYamlFileName();
        @Nullable final Domain domain;

        domain = DataMarshalizerUtil.readFromYaml(locate, Domain.class);
        this.domainService.dataImport(domain);

        return domain;
    }

}