package ru.renessans.jvschool.volkov.tm.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.tm.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.exception.empty.data.EmptyDomainException;

import java.util.Objects;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskUserService taskService;

    @NotNull
    private final IProjectUserService projectService;

    @SneakyThrows
    @Override
    public void dataImport(@Nullable final Domain domain) {
        if (Objects.isNull(domain)) throw new EmptyDomainException();
        this.userService.setAllRecords(domain.getUsers());
        this.taskService.setAllRecords(domain.getTasks());
        this.projectService.setAllRecords(domain.getProjects());
    }

    @SneakyThrows
    @Override
    public void dataExport(@Nullable final Domain domain) {
        if (Objects.isNull(domain)) throw new EmptyDomainException();
        domain.setUsers(this.userService.getAllRecords());
        domain.setTasks(this.taskService.getAllRecords());
        domain.setProjects(this.projectService.getAllRecords());
    }

}