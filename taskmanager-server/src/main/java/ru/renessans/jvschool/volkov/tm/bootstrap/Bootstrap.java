package ru.renessans.jvschool.volkov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.api.endpoint.*;
import ru.renessans.jvschool.volkov.tm.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ISessionRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.*;
import ru.renessans.jvschool.volkov.tm.endpoint.*;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.repository.ProjectUserRepository;
import ru.renessans.jvschool.volkov.tm.repository.SessionRepository;
import ru.renessans.jvschool.volkov.tm.repository.TaskUserRepository;
import ru.renessans.jvschool.volkov.tm.repository.UserRepository;
import ru.renessans.jvschool.volkov.tm.service.*;
import ru.renessans.jvschool.volkov.tm.util.EndpointPublisherUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthenticationService authService = new AuthenticationService(userService);


    @NotNull
    private final ITaskUserRepository taskRepository = new TaskUserRepository();

    @NotNull
    private final ITaskUserService taskService = new TaskUserService(taskRepository);


    @NotNull
    private final IProjectUserRepository projectRepository = new ProjectUserRepository();

    @NotNull
    private final IProjectUserService projectService = new ProjectUserService(projectRepository);


    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IDomainService domainService = new DomainService(
            userService, taskService, projectService
    );

    @NotNull
    private final IDataInterChangeService dataInterChangeService = new DataInterChangeService(
            propertyService, domainService
    );

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(
            sessionRepository, authService, userService, propertyService
    );


    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IAuthenticationEndpoint authEndpoint = new AuthenticationEndpoint(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IAdminDataInterChangeEndpoint adminDomainEndpoint = new AdminDataInterChangeEndpoint(this);

    @NotNull
    private final List<Object> endpoints = Arrays.asList(
            sessionEndpoint, authEndpoint, adminEndpoint, userEndpoint, taskEndpoint, projectEndpoint, adminDomainEndpoint
    );


    @NotNull
    @Override
    public IUserService getUserService() {
        return this.userService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return this.sessionService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return this.authService;
    }

    @NotNull
    @Override
    public ITaskUserService getTaskService() {
        return this.taskService;
    }

    @NotNull
    @Override
    public IProjectUserService getProjectService() {
        return this.projectService;
    }

    @NotNull
    @Override
    public IDataInterChangeService getAdminDataInterChangeService() {
        return this.dataInterChangeService;
    }

    public void run() {
        propertyService.loadProperties();
        initWebServices();
        @NotNull final Collection<User> users = userService.initDemoData();
        taskService.initDemoData(users);
        projectService.initDemoData(users);
    }

    private void initWebServices() {
        @NotNull final String host = this.propertyService.getServerHost();
        @NotNull final Integer port = this.propertyService.getServerPort();
        EndpointPublisherUtil.create(endpoints, host, port);
    }

}