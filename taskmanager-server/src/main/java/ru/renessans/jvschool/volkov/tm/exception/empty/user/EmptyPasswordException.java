package ru.renessans.jvschool.volkov.tm.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

    @NotNull
    private static final String EMPTY_PASSWORD = "Ошибка! Параметр \"пароль\" является пустым или null!\n";

    public EmptyPasswordException() {
        super(EMPTY_PASSWORD);
    }

}