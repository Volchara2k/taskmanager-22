package ru.renessans.jvschool.volkov.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class SignatureUtil {

    @NotNull
    @SneakyThrows
    public String getHashSignature(
            @NotNull final Object value,
            @NotNull final String key,
            @NotNull final Integer cycle
    ) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(value);
        return getHashSignature(json, key, cycle);
    }

    @NotNull
    public String getHashSignature(
            @NotNull final String value,
            @NotNull final String salt,
            @NotNull final Integer cycle
    ) {
        @NotNull String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.hashLineMD5(salt + result + salt);
        }
        return result;
    }

}