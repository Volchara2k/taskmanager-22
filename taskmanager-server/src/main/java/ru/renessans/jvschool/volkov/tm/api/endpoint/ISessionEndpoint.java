package ru.renessans.jvschool.volkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.Session;

import javax.jws.WebMethod;

public interface ISessionEndpoint {

    @NotNull
    Session openSession(
            @Nullable String login,
            @Nullable String password
    );

    boolean closeSession(
            @Nullable Session session
    );

    @Nullable
    Session getUserSession(
            @Nullable Session session
    );

    @NotNull
    Session validateSession(
            @Nullable Session session
    );

    @NotNull
    @WebMethod
    Session validateSessionWithCommandRole(
            @Nullable Session session,
            @Nullable UserRole[] commandRoles
    );

    @NotNull
    SessionValidState verifyValidSessionState(
            @Nullable Session session
    );

    @NotNull
    PermissionValidState verifyValidPermissionState(
            @Nullable Session session,
            @Nullable UserRole[] commandRoles
    );

}