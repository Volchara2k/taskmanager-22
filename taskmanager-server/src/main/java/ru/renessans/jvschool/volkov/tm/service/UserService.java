package ru.renessans.jvschool.volkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.user.*;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.HashUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(
            @NotNull final IUserRepository repository
    ) {
        super(repository);
        this.userRepository = repository;
    }

    @Nullable
    @SneakyThrows
    @Override
    public User getUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        return this.userRepository.getByLogin(login);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        @NotNull final String passwordHash = HashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash);
        return addRecord(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyEmailException();

        @NotNull final String passwordHash = HashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, firstName);
        return addRecord(user);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User addUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole userRole
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(userRole)) throw new EmptyUserRoleException();

        @NotNull final String passwordHash = HashUtil.saltHashLine(password);
        @NotNull final User user = new User(login, passwordHash, userRole);
        return addRecord(user);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User updatePasswordById(
            @Nullable final String id,
            @Nullable final String newPassword
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(newPassword)) throw new EmptyPasswordException();

        @NotNull final String passwordHash = HashUtil.saltHashLine(newPassword);
        @Nullable final User user = super.getRecordByKey(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setPasswordHash(passwordHash);
        return super.updateRecord(user);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();

        @Nullable final User user = super.getRecordByKey(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setFirstName(firstName);
        return super.updateRecord(user);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User editProfileById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();
        if (ValidRuleUtil.isNullOrEmpty(lastName)) throw new EmptyLastNameException();

        @Nullable final User user = super.getRecordByKey(id);
        if (Objects.isNull(user)) throw new EmptyUserException();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        return super.updateRecord(user);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User lockUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        user.setLockdown(true);
        return super.updateRecord(user);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User unlockUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        user.setLockdown(false);
        return super.updateRecord(user);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User deleteUserByLogin(
            @Nullable final String login
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        @Nullable final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        return this.userRepository.deleteByLogin(login);
    }

    @NotNull
    @Override
    public Collection<User> initDemoData() {
        return Arrays.asList(
                addUser(DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD),
                addUser(DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD,
                        DemoDataConst.USER_DEFAULT_FIRSTNAME),
                addUser(DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN)
        );
    }

}