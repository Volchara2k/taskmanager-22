package ru.renessans.jvschool.volkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.api.IRepository;
import ru.renessans.jvschool.volkov.tm.model.Session;

import java.util.Collection;

public interface ISessionRepository extends IRepository<Session> {

    boolean containsUserId(@NotNull final String userId);

    @NotNull
    Collection<Session> getSessionByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

}