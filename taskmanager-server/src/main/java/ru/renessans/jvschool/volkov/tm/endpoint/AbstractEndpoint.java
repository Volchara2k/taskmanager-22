package ru.renessans.jvschool.volkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.api.IServiceLocator;

public abstract class AbstractEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    protected AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}