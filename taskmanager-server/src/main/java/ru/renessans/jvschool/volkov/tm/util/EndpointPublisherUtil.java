package ru.renessans.jvschool.volkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import javax.xml.ws.Endpoint;
import java.util.Collection;

@UtilityClass
public class EndpointPublisherUtil {

    public void create(
            @NotNull final Object endpoint,
            @NotNull final String host,
            @NotNull final Integer port
    ) {
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void create(
            @NotNull final Collection<Object> endpoints,
            @NotNull final String host,
            @NotNull final Integer port
    ) {
        @NotNull String name;
        for (@NotNull final Object endpoint : endpoints) {
            name = endpoint.getClass().getSimpleName();
            @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
            System.out.println(wsdl);
            Endpoint.publish(wsdl, endpoint);
        }
    }

}