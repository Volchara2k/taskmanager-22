package ru.renessans.jvschool.volkov.tm.exception.empty.data;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.tm.exception.AbstractException;

public final class EmptyTaskException extends AbstractException {

    @NotNull
    private static final String EMPTY_TASK = "Ошибка! Параметр \"задача\" является null!\n";

    public EmptyTaskException() {
        super(EMPTY_TASK);
    }

}